Analytics Frontend
============

Dockerfile

    FROM node:4.4.1

    RUN mkdir -p /var/www

    WORKDIR /var/www

    COPY package.json .
    COPY webpack.production.config.js .
    COPY server-prod.js .

    ADD entrypoint.sh /sbin/entrypoint.sh
    RUN chmod 755 /sbin/entrypoint.sh
    ENTRYPOINT ["/sbin/entrypoint.sh"]

entrypoint.sh

    #!/bin/bash

    echo
    echo "Installing packages..."
    npm install

    if [ "$NODE_ENV" = "production" ]
    then
      echo
      echo "Building app..."
      npm run build

      echo
      echo "Serving app in production mode..."
      npm run serve
    else
      echo
      echo "Running app..."
      npm start
    fi

docker-compose.yml

    version: '2'

    services:

      ids_frontend:
        build:
          context: .
        volumes:
          - node_modules:/var/www/node_modules
          - ./src:/var/www/src
        ports:
          - "9001:9001"
        environment:
          - NODE_ENV=development
          - EVE_BACKEND_HOST=10.0.9.118
          - EVE_BACKEND_PORT=8000
          - NPM_CONFIG_LOGLEVEL=warn
          - NPM_CONFIG_SAVE=true
          - NPM_CONFIG_SAVE_EXACT=true
          - NPM_CONFIG_REGISTRY=http://registry.npmjs.org/
          - NPM_CONFIG_PROXY=http://10.0.9.23:3143
          - PORT=9001

    volumes:

      node_modules:
        driver: local
