import moment from 'moment'
import { InitalConstants } from '../../../constants'

const initalState = {
  from: InitalConstants.DATE_RANGE.from,
  to: InitalConstants.DATE_RANGE.to
}

export default function date_range(state=initalState, action={}) {
  switch (action.type) {
  case 'DATE_RANGE_SET':
    return {
      ...state,
      ...action.payload
    }
  case 'DATE_RANGE_SET_BY_PERIOD':
    const { period, unit } = action.payload
    return {
      from: moment().subtract(period, unit).startOf('minute').toDate(),
      to: new Date,
    }
  default:
    return state
  }
}
