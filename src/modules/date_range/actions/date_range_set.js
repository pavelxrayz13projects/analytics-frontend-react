export default function dateRangeSet(payload) {
  return {
    type: 'DATE_RANGE_SET',
    payload
  }
}
