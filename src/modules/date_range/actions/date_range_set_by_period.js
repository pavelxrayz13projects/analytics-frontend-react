export default function dateRangeSetByPeriod(payload) {
  return {
    type: 'DATE_RANGE_SET_BY_PERIOD',
    payload
  }  
}
