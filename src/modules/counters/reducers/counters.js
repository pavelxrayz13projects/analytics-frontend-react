const counters = (state = { _items: [], isFetching: false }, action) => {
  switch (action.type) {
    case 'RECEIVE_COUNTERS':
      return {
        ...state,
        ...action.payload
      }
    case 'SET_COUNTERS_FETCHING':
      return {
        ...state,
        isFetching: action.isFetching
      }
    default:
      return state;
  }
}

export default counters;