import axios from 'axios'
import countersReceive from './counters_receive'
import appStateSetError from '../../app_state/actions/app_state_set_error'

const countersFetch = (date_range) => {
  return (dispatch) => {
    axios.post('/api/counters', date_range)
      .then(response => {
        const { data } = response
        dispatch(countersReceive({ isFetching: false, ...data }))
      })
      .catch(err => {
        dispatch(countersReceive({ isFetching: false, _items: [] }))
        dispatch(appStateSetError(err));
      });
  }
}

export default countersFetch;
