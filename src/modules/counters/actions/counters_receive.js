const dataReceive = (payload = {}) => {
  return {
    type: 'RECEIVE_COUNTERS',
    payload
  }
}

export default dataReceive;