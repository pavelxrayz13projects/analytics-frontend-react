import axios from 'axios'
import counters1Receive from './counters1_receive'
import appStateSetError from '../../app_state/actions/app_state_set_error'

const counters1Fetch = (date_range) => {
  return (dispatch) => {
    axios.post('/api/counters', date_range)
      .then(response => {
        const { data } = response
        dispatch(counters1Receive({ isFetching: false, ...data }))
      })
      .catch(err => {
        dispatch(counters1Receive({ isFetching: false, _items: [] }))
        dispatch(appStateSetError(err));
      });
  }
}

export default counters1Fetch;
