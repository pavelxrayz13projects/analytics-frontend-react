const initalState = {
  data: []
}

export default function incidentsChart(state=initalState, action={}) {
  switch (action.type) {
    case 'INCIDENTS_CHART_SET_DATA':
      return {
        ...state,
        data: action.payload || initalState.data
      }
    default:
      return state
  }
}
