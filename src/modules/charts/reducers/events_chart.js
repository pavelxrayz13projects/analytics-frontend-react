const initalState = {
  data: []
}

export default function eventsChart(state=initalState, action={}) {
  switch (action.type) {
    case 'EVENTS_CHART_SET_DATA':
      return {
        ...state,
        data: action.payload || initalState.data
      }
    default:
      return state
  }
}
