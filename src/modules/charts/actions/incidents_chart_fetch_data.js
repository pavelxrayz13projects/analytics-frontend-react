import axios from 'axios'
import incidentsChartSetData from './incidents_chart_set_data'
import appStateSetError from '../../app_state/actions/app_state_set_error'

const incidentsChartFetchData = (params) => {
  return (dispatch) => {
    axios.post('/api/incidents_histogram', params)
      .then(response => {
        const { data } = response;
        dispatch(incidentsChartSetData(data))
      })
      .catch(err => {
        dispatch(incidentsChartSetData())
        dispatch(appStateSetError(err))
      });
  }
}

export default incidentsChartFetchData
