export default function eventsChartSetData(payload) {
  return {
    type: 'EVENTS_CHART_SET_DATA',
    payload
  }
}
