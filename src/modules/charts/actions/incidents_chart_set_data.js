export default function incidentsChartSetData(payload) {
  return {
    type: 'INCIDENTS_CHART_SET_DATA',
    payload
  }
}
