import axios from 'axios'
import eventsChartSetData from './events_chart_set_data'
import appStateSetError from '../../app_state/actions/app_state_set_error'

const eventsChartFetchData = (params) => {
  return (dispatch) => {
    axios.post('/api/events_histogram', params)
      .then(response => {
        const { data } = response;
        dispatch(eventsChartSetData(data))
      })
      .catch(err => {
        dispatch(eventsChartSetData())
        dispatch(appStateSetError(err))
      });
  }
}

export default eventsChartFetchData
