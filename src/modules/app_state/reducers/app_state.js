import { InitalConstants } from '../../../constants'

const initalState = {
  status: 200,
  error: undefined,
}

const app_state = (state = initalState, action) => {
  switch (action.type) {
  case 'SET_APP_STATE':
    return {
      ...state,
      ...action.state
    }
  case 'SET_APP_STATE_OK':
    return {
      ...state,
      status: 200,
      error: undefined
    }
  case 'SET_APP_STATE_ERROR':
    const { error } = action;
    return {
      ...state,
      status: error.status || 500,
      error
    }
  default:
    return state;
  }
}

export default app_state;
