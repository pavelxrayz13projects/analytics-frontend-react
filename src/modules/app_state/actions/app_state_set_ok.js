const appSetStateOk = () => {
  return {
    type: 'SET_APP_STATE_OK'
  }
}

export default appSetStateOk;