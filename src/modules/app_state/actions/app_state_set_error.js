const appSetStateError = (error = {}) => {
  return {
    type: 'SET_APP_STATE_ERROR',
    error
  }
}

export default appSetStateError;