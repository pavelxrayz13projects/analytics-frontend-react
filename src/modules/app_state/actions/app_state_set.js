const appStateSet = (state) => {
  return {
    type: 'SET_APP_STATE',
    state
  }
}

export default appStateSet;