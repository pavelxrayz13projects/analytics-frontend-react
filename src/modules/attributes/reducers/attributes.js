import { InitalAttributes } from '../../../constants';

const buildIndex = (attributes) => {
  return attributes.map((attr, index) => {
    return {
      ...attr,
      index: index * 10
    }
  })
}

const setState = (endpoint, state = []) => {
  state = buildIndex(state);
  localStorage.setItem(`${endpoint}_attributes`, JSON.stringify(state));
  return state;
}

const loadAttributes = (endpoint) => {
  if (!localStorage.hasOwnProperty(`${endpoint}_attributes`)) return false;
  return JSON.parse(localStorage.getItem(`${endpoint}_attributes`));
}

const initalState = {
  events:           loadAttributes('events') || InitalAttributes.EVENT_ATTRIBUTES,
  incidents:        loadAttributes('incidents') || InitalAttributes.INCIDENT_ATTRIBUTES,
  fired_signatures: loadAttributes('fired_signatures') || InitalAttributes.FIRED_SIGNATURES_ATTRIBUTES,
  webproxy:         loadAttributes('webproxy') || InitalAttributes.WEBPROXY_ATTRIBUTES,
  firewall:         loadAttributes('firewall') || InitalAttributes.FIREWALL_ATTRIBUTES,
}

const attributes = (state = initalState, action) => {
  switch (action.type) {
    case 'PLACE_ATTRIBUTE_AFTER':
      // example:     source  target
      //                   ↓  ↓
      // indexes before: 0 10 20 30
      // indexes after:  0 25 20 30
      // after sorting:  0 20 25 30
      // profit!
      const target_index = _(state[action.endpoint]).findWhere({ id: action.target }).index;
      const res = state[action.endpoint].map(attribute => {
        if (attribute.id !== action.source) return attribute;
        return {
          ...attribute,
          index: target_index + 5 // -5 if we want to place source before target
        }
      });
      return {
        ...state,
        [action.endpoint]: setState(action.endpoint, _(res).sortBy('index'))
      }
    case 'SET_ATTRIBUTES':
      return {
        ...state,
        [action.endpoint]: setState(action.endpoint, action.payload)
      }
    case 'TOGGLE_VISIBILITY':
      return {
        ...state,
        [action.endpoint]: setState(action.endpoint, state[action.endpoint].map(attribute => {
          if (attribute.id !== action.id) return attribute;
          return {
            ...attribute,
            visibility: !attribute.visibility
          }
        }))
      }
    case 'SET_NEW_ATTRIBUTE_SIZE':
      return {
        ...state,
        [action.endpoint]: setState(action.endpoint, state[action.endpoint].map(attribute => {
          if (attribute.id !== action.payload.id) return attribute;
          return {
            ...attribute,
            width: action.payload.newSize
          }
        }))
      }
    default:
      return state;
  }
};

export default attributes;
