const attributePlaceAfter = (endpoint, source, target) => {
  return {
    type: 'PLACE_ATTRIBUTE_AFTER',
    endpoint,
    source,
    target
  }
}

export default attributePlaceAfter;
