const attributeToggle = (endpoint, id) => {
  // console.log(id);
  return {
    type: 'TOGGLE_VISIBILITY',
    endpoint,
    id
  }
};

export default attributeToggle;
