const attributesSet = (endpoint, payload = []) => {
  return {
    type: 'SET_ATTRIBUTES',
    endpoint,
    payload
  }
};

export default attributesSet;
