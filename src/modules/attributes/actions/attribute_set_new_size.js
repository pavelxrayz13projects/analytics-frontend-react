const attributeSetNewSize = (endpoint, id, newSize) => {
  return {
    type: 'SET_NEW_ATTRIBUTE_SIZE',
    endpoint,
    payload: {
      id,
      newSize
    }
  }
}

export default attributeSetNewSize
