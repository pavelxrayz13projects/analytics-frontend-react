export default function sensorsSet(payload) {
  return {
    type: 'SENSORS_SET',
    payload
  }
}
