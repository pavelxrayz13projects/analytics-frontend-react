export default function sensorToggle(payload) {
  return {
    type: 'SENSOR_TOGGLE',
    payload
  }
}
