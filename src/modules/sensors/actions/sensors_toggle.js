export default function sensorsToggle(payload) {
  return {
    type: 'SENSORS_TOGGLE',
    payload
  }
}
