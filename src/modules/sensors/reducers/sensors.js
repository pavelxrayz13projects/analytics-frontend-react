import organizations from '../../organizations/reducers/organizations'
import { toggleArray, getSensors } from '../../utils'

const initalState = getSensors(organizations())

export default function sensors(state = initalState, action = {}) {
  switch (action.type) {
  case 'SENSORS_SET':
    return [
      ...state,
      ...action.payload
    ]
  case 'SENSOR_TOGGLE':
    return toggleArray(state, action.payload)
  case 'SENSORS_TOGGLE':
    // console.log(action.payload);
    // console.log(_(state).intersection(action.payload));
    if (_(state).intersection(action.payload).length > 0) {
      return _(state).difference(action.payload)
    } else {
      return _(state).union(action.payload)
    }
    return state
  default:
    return state
  }
}
