const initalState = {
  _items: []
}

export default function organizations(state = initalState, action = {}) {
  switch (action.type) {
  case 'SET_ORGANIZATIONS':
    return {
      ...state,
      ...action.payload
    }
    default:
      return state
  }
}
