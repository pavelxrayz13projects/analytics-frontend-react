import axios from 'axios'
import organizationsSet from './organizations_set'
import sensorsSet from '../../sensors/actions/sensors_set'
import appStateSetError from '../../app_state/actions/app_state_set_error'
import { getSensors } from '../../utils'

export default function organizationsFetch(params = {}) {
  return (dispatch) => {
    axios.post('/api/organizations', params)
      .then(response => {
        const { data } = response
        dispatch(organizationsSet(data))
        dispatch(sensorsSet(getSensors(data)))
      })
      .catch(err => {
        dispatch(organizationsSet({ _items: [] }))
        dispatch(appStateSetError(err.data))
      })
  }
}
