export default function organizationsSet(payload) {
  return {
    type: 'SET_ORGANIZATIONS',
    payload
  }
}
