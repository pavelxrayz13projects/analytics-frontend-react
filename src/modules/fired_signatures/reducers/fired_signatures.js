import group from '../../endpoints/reducers/group'

const initalState = {
  _items: [],
  _meta: {
    choices: {
      direction: ["src", "dst", "both"],
      proto: ["TCP", "UDP", "ICMP"],
      priority: [1, 2, 3],
      group: ["info","scan","auth","policy","malware","ddos","attacks","other"]
    }
  },
  page: 1,
  max_results: 10,
  aggregate: {
    group: group()
  }
}

const fired_signatures = (state=initalState, action) => {
  const nextAggr = { ...state.aggregate }
  switch (action.type) {
  case 'FS_SET_PAGE':
    return {
      ...state,
      page: action.payload
    }
  case 'FS_SET_AGGREGATE':
  return {
    ...state,
    aggregate: {
      ...state.aggregate,
      ...action.payload
    }
  }
  case 'FS_REMOVE_AGGREGATE':
  if (action.payload === 'group') {
    nextAggr.group = group()
  } else {
    delete nextAggr[action.payload]
  }
  return {
    ...state,
    aggregate: nextAggr
  }
  case 'FS_SET_STATE':
  action.payload = action.payload || initalState
  return {
    ...state,
    _items: action.payload._items,
    _meta: {
      ...state._meta,
      choices: {
        ...state._meta.choices,
        ...action.payload._meta.choices
      }
    }
  }
  case 'FS_SET_SORT':
  return {
    ...state,
    aggregate: {
      ...state.aggregate,
      "$sort": action.payload
    }
  }
  case 'FS_REMOVE_SORT':
  delete nextAggr["$sort"]
  return {
    ...state,
    aggregate: nextAggr
  }
  case 'FS_SET_MAX_RESULTS':
  return {
    ...state,
    max_results: action.payload
  }
  case 'FS_TOGGLE_GROUP':
  return {
    ...state,
    aggregate: {
      ...state.aggregate,
      group: group(state.aggregate.group, action)
    }
  }
  default:
    return state;
  }
}

export default fired_signatures;
