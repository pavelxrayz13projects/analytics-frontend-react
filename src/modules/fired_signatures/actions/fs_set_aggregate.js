const fs_set_aggregate = (payload) => {
  return {
    type: 'FS_SET_AGGREGATE',
    payload
  }
}

export default fs_set_aggregate;
