const fsRemoveSort = (payload) => {
  return {
    type: 'FS_REMOVE_SORT',
    payload
  }
}

export default fsRemoveSort
