const fs_set_state = (payload) => {
  return {
    type: 'FS_SET_STATE',
    payload
  }
}

export default fs_set_state;
