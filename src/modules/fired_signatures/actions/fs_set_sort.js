const fsSetSort = (payload) => {
  return {
    type: 'FS_SET_SORT',
    payload
  }
}

export default fsSetSort
