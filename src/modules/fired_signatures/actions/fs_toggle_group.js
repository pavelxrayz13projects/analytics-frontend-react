const fsToggleGroup = (group) => {
  return {
    type: 'FS_TOGGLE_GROUP',
    group
  }
}

export default fsToggleGroup;
