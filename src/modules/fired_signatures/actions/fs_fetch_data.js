import axios from 'axios';
import fsSetState from './fs_set_state';
import appStateSetOk from '../../app_state/actions/app_state_set_ok';
import appStateSetError from '../../app_state/actions/app_state_set_error';

const fsFetchData = (params) => {
  return (dispatch) => {
    //dispatch(dataReceive(endpoint, { isFetching: true }));
    //dispatch(appStateSetOk());
    axios.post('/api/fired_signatures', params)
      .then(response => {
        const { data } = response;
        // console.log('data: ', data);
        dispatch(fsSetState(data));
        // dispatch(appStateSetOk());
      })
      .catch(err => {
        // console.log('error: ', err.data);
        dispatch(fsSetState());
        dispatch(appStateSetError(err));
      });
  }
}

export default fsFetchData;
