const fsRemoveAggregate = (payload) => {
  return {
    type: 'FS_REMOVE_AGGREGATE',
    payload
  }
}

export default fsRemoveAggregate
