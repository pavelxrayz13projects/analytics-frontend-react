const fs_set_page = (payload) => {
  return {
    type: 'FS_SET_PAGE',
    payload
  }
}

export default fs_set_page;
