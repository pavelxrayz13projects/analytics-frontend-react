const fsSetMaxResults = (payload) => {
  return {
    type: 'FS_SET_MAX_RESULTS',
    payload
  }
}

export default fsSetMaxResults
