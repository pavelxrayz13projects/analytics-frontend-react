export default function active_tab(state=0, action={}) {
  switch (action.type) {
  case 'ACTIVE_TAB_SET':
    return action.payload
  default:
    return state
  }
}
