export default function activeTabSet(payload) {
  return {
    type: 'ACTIVE_TAB_SET',
    payload
  }
}
