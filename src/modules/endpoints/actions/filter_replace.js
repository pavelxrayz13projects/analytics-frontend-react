const filterReplace = (endpoint, payload) => {
  return {
    type: 'REPLACE_FILTER',
    endpoint,
    payload
  }
}

export default filterReplace;
