const groupToggle = (group) => {
  return {
    type: 'TOGGLE_GROUP_FILTER',
    endpoint: 'events',
    group
  }
}

export default groupToggle;
