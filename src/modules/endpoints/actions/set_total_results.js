const setTotalResults = (totalResults = 0) => {
  return {
    type: 'SET_TOTAL_RESULTS',
    totalResults
  }
}

export default setTotalResults;