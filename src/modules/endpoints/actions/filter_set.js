const filterSet = (endpoint, column, filter = {}) => {
  return {
    type: 'SET_FILTER',
    endpoint,
    column,
    filter
  }
}

export default filterSet;
