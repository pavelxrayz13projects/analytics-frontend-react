const filterRemove = (endpoint, column) => {
  return {
    type: 'REMOVE_FILTER',
    endpoint,
    column
  }
}

export default filterRemove;
