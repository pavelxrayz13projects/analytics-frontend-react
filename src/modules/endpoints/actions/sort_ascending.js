const sortAscending = (endpoint, column) => {
  return {
    type: 'SORT_ASC',
    endpoint,
    column
  }
}

export default sortAscending;
