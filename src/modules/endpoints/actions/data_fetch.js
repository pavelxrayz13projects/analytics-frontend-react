import axios from 'axios';

// Actions
import dataReceive from './data_receive';
import setFetching from './set_fetching';
import appStateSetOk from '../../app_state/actions/app_state_set_ok';
import appStateSetError from '../../app_state/actions/app_state_set_error';

const dataFetch = (endpoint, { page=1, max_results=25, sort={}, filter }) => {
  return (dispatch) => {
    if (['firewall', 'webproxy'].indexOf(endpoint) !== -1) {
      dispatch(setFetching(endpoint));
    }
    dispatch(appStateSetOk());
    axios.post(`/api/endpoints/${endpoint}`, { page, max_results, sort, filter })
      .then(response => {
        const { data } = response;
        // console.log('data: ', data);
        dispatch(dataReceive(endpoint, data));
        dispatch(setFetching(endpoint, false));
        dispatch(appStateSetOk());
      })
      .catch(err => {
        // console.log('error: ', err.data);
        dispatch(dataReceive(endpoint, { _items: [] }));
        dispatch(setFetching(endpoint, false));
        dispatch(appStateSetError(err));
      });
  }
}

export default dataFetch;
