const dataReceive = (endpoint, payload = {}) => {
  return {
    type: 'RECEIVE_DATA',
    endpoint,
    payload
  }
}

export default dataReceive;
