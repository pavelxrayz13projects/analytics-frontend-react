const pageSet = (endpoint, payload = 1) => {
  return {
    type: 'SET_PAGE',
    endpoint,
    payload
  }
}

export default pageSet;
