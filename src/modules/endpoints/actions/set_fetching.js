const setFetching = (endpoint, isFetching = true) => {
  return {
    type: 'SET_FETCHING',
    endpoint,
    isFetching
  }
}

export default setFetching;
