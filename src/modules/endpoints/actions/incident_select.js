export default function incidentSelect(payload) {
  return {
    type: 'INCIDENT_SELECT',
    endpoint: 'incidents',
    payload
  }
}
