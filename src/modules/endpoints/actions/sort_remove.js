const sortRemove = (endpoint, column) => {
  return {
    type: 'SORT_REMOVE',
    endpoint,
    column
  }
}

export default sortRemove;
