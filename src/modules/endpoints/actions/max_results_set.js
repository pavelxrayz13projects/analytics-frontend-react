// max_results_set.js
const maxResultsSet = (endpoint, payload) => {
  return {
    type: 'SET_MAX_RESULTS',
    endpoint,
    payload
  }
}

export default maxResultsSet;
