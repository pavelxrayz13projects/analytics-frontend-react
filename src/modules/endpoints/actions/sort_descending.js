const sortDescending = (endpoint, column) => {
  return {
    type: 'SORT_DESC',
    endpoint,
    column
  }
}

export default sortDescending;
