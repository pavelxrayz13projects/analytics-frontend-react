import params from './params';
import data from './data';
import date_range from '../../date_range/reducers/date_range'
import moment from 'moment'

const initalEndpointState = {
  isFetching: false,
  data: data(),
  params: params()
}

const initalState = {
  events: {
    ...initalEndpointState,
    params: {
      ...initalEndpointState.params,
      filter: {
        ...initalEndpointState.params.filter,
        logdate: { $gte: date_range().from, $lte: date_range().to },
      }
    }
  },
  incidents: {
    ...initalEndpointState,
    selectedId: null,
    params: {
      ...initalEndpointState.params,
      filter: {
        ...initalEndpointState.params.filter,
        created: { $gte: moment(date_range().to).subtract(1, 'days').toDate(), $lte: date_range().to },
      }
    }
  },
  webproxy: {
    ...initalEndpointState
  },
  firewall: {
    ...initalEndpointState
  }
}

const endpoints = (state = initalState, action) => {
  switch (action.type) {
  case 'SET_STATE':
    return {
      ...state,
      ...action.payload
    }
  case 'SET_FETCHING':
    return {
      ...state,
      [action.endpoint]: {
        ...state[action.endpoint],
        isFetching: action.isFetching
      }
    }
  case 'RECEIVE_DATA':
    return {
      ...state,
      [action.endpoint]: {
        ...state[action.endpoint],
        data: data(state[action.endpoint].data, action)
      }
    }
  case 'INCIDENT_SELECT':
    return {
      ...state,
      [action.endpoint]: {
        ...state[action.endpoint],
        selectedId: action.payload
      }
    }
  case 'SET_FILTER':
  case 'REMOVE_FILTER':
  case 'SET_PAGE':
  case 'SET_MAX_RESULTS':
  case 'SORT_ASC':
  case 'SORT_DESC':
  case 'SORT_REMOVE':
  case 'TOGGLE_GROUP_FILTER':
  case 'REPLACE_FILTER':
    return {
      ...state,
      [action.endpoint]: {
        ...state[action.endpoint],
        params: params(state[action.endpoint].params, action)
      }
    }
  default:
    return state
  }
}

export default endpoints;
