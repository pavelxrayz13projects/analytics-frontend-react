const sort = (state = {}, action = {}) => {
  const next_state = {...state};
  switch (action.type) {
    case 'SORT_ASC':
      next_state[action.column] = 1;
      return next_state;
    case 'SORT_DESC':
      next_state[action.column] = -1;
      return next_state;
    case 'SORT_REMOVE':
      delete next_state[action.column];
      return next_state;
    default:
      return state;
  }
};

export default sort;
