import moment from 'moment'
import group from './group';

const initalState = {
  group: group(),
}

const filter = (state = initalState, action = {}) => {
  switch (action.type) {
    case 'SET_FILTER':
      return {
        ...state,
        [action.column]: action.filter
      }
    case 'REMOVE_FILTER':
      const nextState = { ...state };
      delete nextState[action.column];
      return nextState;
    case 'TOGGLE_GROUP_FILTER':
      return {
        ...state,
        group: group(state.group, action)
      }
    case 'REPLACE_FILTER':
      return {
        ...initalState,
        ...action.payload
      }
    default:
      return state;
  }
};

export default filter;
