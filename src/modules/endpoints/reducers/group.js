const initalState = {
  $in: []
}

const toggleGroup = (groups, group) => {
  const nextState = groups.filter(i => i !== group);
  if (nextState.length < groups.length) return nextState; // remove item
  return [ // add item
    ...groups,
    group
  ]
}

const group = (state = initalState, action = {}) => {
  switch (action.type) {
  case 'TOGGLE_GROUP_FILTER':
  case 'FS_TOGGLE_GROUP':
    return {
      ...state,
      $in: toggleGroup(state.$in, action.group)
    }
  default:
    return state;
  }
}

export default group;
