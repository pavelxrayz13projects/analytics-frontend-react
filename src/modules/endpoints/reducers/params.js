import filter from './filter';
import sort from './sort';

const initalState = {
  page: 1,
  max_results: 10,
  sort: sort(),
  filter: filter()
}

const params = (state = initalState, action = {}) => {
  switch (action.type) {
  case 'SET_PAGE':
    return {
      ...state,
      page: action.payload
    }
  case 'SET_MAX_RESULTS':
    return {
      ...state,
      max_results: action.payload
    }
  case 'SET_FILTER':
  case 'REMOVE_FILTER':
  case 'TOGGLE_GROUP_FILTER':
  case 'REPLACE_FILTER':
    return {
      ...state,
      filter: filter(state.filter, action)
    }
  case 'SORT_ASC':
  case 'SORT_DESC':
  case 'SORT_REMOVE':
    return {
      ...state,
      sort: sort(state.sort, action)
    }
  default:
    return state
  }
}

export default params
