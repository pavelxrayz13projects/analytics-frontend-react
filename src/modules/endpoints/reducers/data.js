const initalState = {
  _items: [],
  _meta: {
    choices: {
      priority: [1, 2, 3],
      proto: ["TCP", "UDP", "ICMP"],
      group: ["info","scan","auth","policy","malware","ddos","attacks","other"]
    }
  },
  _links: {},
}

const data = (state = initalState, action = {}) => {
  switch (action.type) {
  case 'RECEIVE_DATA':
    return {
      ...state,
      ...action.payload
    }
  default:
    return state
  }
}

export default data
