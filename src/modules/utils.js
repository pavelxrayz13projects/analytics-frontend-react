export const toggleArray = (array, item) => {
  const filtered = array.filter(i => i !== item);
  if (filtered.length < array.length) return filtered; // remove item
  return [ // add item
    ...array,
    item
  ]
}

export const getSensors = (obj) => {
  let _res = []
  const searchLoop = (o) => {
    if (o.sensors) {
      o.sensors.map(sensor => _res.push(sensor.address))
    } else {
      (o._items || o.branches || o.segments).map(ob => searchLoop(ob))
    }
  }
  searchLoop(obj)
  return _res
}
