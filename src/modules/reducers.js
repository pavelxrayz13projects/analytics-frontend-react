import { combineReducers } from 'redux'
import endpoints from './endpoints/reducers/endpoints'
import attributes  from './attributes/reducers/attributes'
import app_state from './app_state/reducers/app_state'
import counters from './counters/reducers/counters'
import counters1 from './counters/reducers/counters1'
import groups from './groups/reducers/groups'
import fired_signatures from './fired_signatures/reducers/fired_signatures'
import organizations from './organizations/reducers/organizations'
import sensors from './sensors/reducers/sensors'
import eventsChart from './charts/reducers/events_chart'
import incidentsChart from './charts/reducers/incidents_chart'
import date_range from './date_range/reducers/date_range'
import active_tab from './active_tab/reducers/active_tab'

const reducers = combineReducers({
  endpoints,
  attributes,
  app_state,
  counters,
  counters1,
  groups,
  fired_signatures,
  organizations,
  sensors,
  eventsChart,
  incidentsChart,
  date_range,
  active_tab
});

export default reducers;
