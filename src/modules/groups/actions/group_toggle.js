export default function groupToggle(payload) {
  return {
    type: 'GROUP_TOGGLE',
    payload
  }
}
