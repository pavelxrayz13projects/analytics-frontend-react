import { toggleArray } from '../../utils'

export default function groups(state=[], action={}) {
  switch (action.type) {
    case 'GROUP_TOGGLE':
      return toggleArray(state, action.payload)
    default:
      return state
  }
}
