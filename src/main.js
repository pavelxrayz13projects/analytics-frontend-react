// import injectTapEventPlugin from 'react-tap-event-plugin';
import styles from './public/assets/css/index.css';

import React from 'react';
import ReactDOM from 'react-dom';

import AppContainer from './containers/App.js';

// injectTapEventPlugin();

ReactDOM.render(
  <AppContainer />,
  document.getElementById('root')
);
