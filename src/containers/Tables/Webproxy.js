import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { mapEndpointStateToProps, mapEndpointDispatchToProps } from './utils'

// Widgets
import Table from '../../widgets/Table/table';

const ENDPOINT = 'webproxy';

const mapStateToProps = (state) => {
  return {
    ...mapEndpointStateToProps(ENDPOINT, state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...mapEndpointDispatchToProps(ENDPOINT, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
