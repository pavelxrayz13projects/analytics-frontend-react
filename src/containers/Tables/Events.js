import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import moment from 'moment';

import { InitalConstants } from '../../constants';
import { mapEndpointStateToProps, mapEndpointDispatchToProps } from './utils'

// Widgets
import Table from '../../widgets/Table/table';

// Actions
import filterSet from '../../modules/endpoints/actions/filter_set';
import filterReplace from '../../modules/endpoints/actions/filter_replace';

const ENDPOINT = 'events';

const mapStateToProps = (state) => {
  return {
    ...mapEndpointStateToProps(ENDPOINT, state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...mapEndpointDispatchToProps(ENDPOINT, dispatch),
    handleDateRangeChange(date_range) {
      return dispatch(filterSet('events', 'logdate', {
        $gte: date_range.from,
        $lte: date_range.to
      }))
    },
    handleRowClick(e, data) {
      const logdate = new Date(data.logdate);
      const logTime = {
        '$gte': moment(logdate).subtract(300, 'seconds').toDate(),
        '$lte': moment(logdate).add(300, 'seconds').toDate()
      }
      if (data.homenet_address.indexOf(data.src) === -1) {
        dispatch(filterReplace('firewall', {logTime, SourceIP: data.src}));
        dispatch(filterReplace('webproxy', {logTime, ClientIP: data.src}));
      } else if (data.homenet_address.indexOf(data.dst) === -1) {
        dispatch(filterReplace('firewall', {logTime, DestanationIP: data.dst}));
        dispatch(filterReplace('webproxy', {logTime, DestHostIP: data.dst}));
      }
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
