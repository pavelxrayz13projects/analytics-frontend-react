import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { InitalConstants } from '../../constants';

// Widgets
import Table from '../../widgets/Table/table';

// Actions
import attributeToggle from '../../modules/attributes/actions/attribute_toggle';
import attributePlaceAfter from '../../modules/attributes/actions/attribute_place_after';
import attributeSetNewSize from '../../modules/attributes/actions/attribute_set_new_size';

import fsSetMaxResults from '../../modules/fired_signatures/actions/fs_set_max_results';
import fsSetPage from '../../modules/fired_signatures/actions/fs_set_page'
import fsFetchData from '../../modules/fired_signatures/actions/fs_fetch_data';
import fsSetAggregate from '../../modules/fired_signatures/actions/fs_set_aggregate';
import fsRemoveAggregate from '../../modules/fired_signatures/actions/fs_remove_aggregate';
import fsSetSort from '../../modules/fired_signatures/actions/fs_set_sort';
import fsRemoveSort from '../../modules/fired_signatures/actions/fs_remove_sort';

import filterSet from '../../modules/endpoints/actions/filter_set';
import filterRemove from '../../modules/endpoints/actions/filter_remove';

const ENDPOINT = 'fired_signatures';

const mapStateToProps = ({ date_range, fired_signatures, attributes }) => {
  return {
    prefix: 'fired_signatures',
    date_range,
    attributes: attributes.fired_signatures,
    columns: _(attributes.fired_signatures).select(attr => attr.visibility === true),
    items: fired_signatures._items,
    params: {
      aggregate: {
        ...fired_signatures.aggregate,
        $from: date_range.from,
        $to: date_range.to,
      },
      max_results: fired_signatures.max_results,
      page: fired_signatures.page,
    },
    isSortedAsc(column) {
      if (typeof fired_signatures.aggregate["$sort"] === 'undefined') return false
      return fired_signatures.aggregate["$sort"][column.id] === 1
    },
    isSortedDesc(column) {
      if (typeof fired_signatures.aggregate["$sort"] === 'undefined') return false
      return fired_signatures.aggregate["$sort"][column.id] === -1
    },
    isFiltered(column) {
      if (column.id === 'group') {
        return fired_signatures.aggregate.group.$in.length > 0
      }
      return typeof fired_signatures.aggregate[column.id] !== 'undefined'
    },
    getFilter(column) {
      return fired_signatures.aggregate[column.id]
    },
    getChoices(column) {
      if (column.filter_type !== 'subset') return false
      return fired_signatures._meta.choices[column.id]
    },
    pagination: {
      total: NaN,
      max_results: fired_signatures.max_results,
      page: fired_signatures.page,
      next: fired_signatures.page+1,
      prev: ((fired_signatures.page - 1) <= 1 ? 1 : (fired_signatures.page - 1)),
      last: NaN,
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleRowClick(e, data, isSameRow) {
      if (isSameRow) return dispatch(filterRemove('events', 'rule'));
      return dispatch(filterSet('events', 'rule', data.rule));
    },
    handleDateRangeChange(date_range) {
      return dispatch(fsSetAggregate({
        $from: date_range.from,
        $to: date_range.to
      }))
    },
    handleFilterForm(column, data) {
      dispatch(fsSetPage(1))
      return dispatch(fsSetAggregate({[column.id]: data}))
    },
    handleResetFilterForm(column) {
      dispatch(fsSetPage(1))
      return dispatch(fsRemoveAggregate(column.id))
    },
    dataFetch(params = { aggregate: { $from: InitalConstants.DATE_RANGE.from, $to: InitalConstants.DATE_RANGE.to } }) {
      dispatch(fsFetchData(params));
    },
    moveColumn(source, target) {
      dispatch(attributePlaceAfter(ENDPOINT, source, target));
    },
    resizeColumn(id, newSize) {
      dispatch(attributeSetNewSize(ENDPOINT, id, newSize))
    },
    sortAscending(column, isSortedAsc=false) {
      if (isSortedAsc) return dispatch(fsRemoveSort(column.id))
      return dispatch(fsSetSort({[column.id]: 1}))
    },
    sortDescending(column, isSortedDesc=false) {
      if (isSortedDesc) return dispatch(fsRemoveSort(column.id))
      return dispatch(fsSetSort({[column.id]: -1}))
    },
    removeSorting(column) {
      return dispatch(fsRemoveSort(column.id))
    },
    attrVisibilityToggle(e, id) {
      dispatch(attributeToggle(ENDPOINT, id));
    },
    setPage(page) {
      if(isNaN(page)) return false;
      dispatch(fsSetPage(page))
    },
    setMaxResults(maxResults) {
      dispatch(fsSetMaxResults(maxResults));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
