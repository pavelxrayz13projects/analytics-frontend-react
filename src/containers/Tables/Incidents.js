import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import classnames from 'classnames'
import moment from 'moment'

import { InitalConstants } from '../../constants'
import { mapEndpointStateToProps, mapEndpointDispatchToProps } from './utils'

// Widgets
import Table from '../../widgets/Table/table'
import Card from '../../widgets/IncidentCard/card'

// Actions
import filterSet from '../../modules/endpoints/actions/filter_set'
import incidentSelect from '../../modules/endpoints/actions/incident_select'
import activeTabSet from '../../modules/active_tab/actions/active_tab_set'

const saveIdToLocalStorage = (id) => {
  let ids = []
  if (localStorage.hasOwnProperty('viewed_incidents_ids')) {
    ids = JSON.parse(localStorage.getItem('viewed_incidents_ids'))
  }
  if (ids.indexOf(id) !== -1) return // there is already one
  if (ids.length > 1000) ids.shift() // ids list length limited to 1000
  ids.push(id)
  localStorage.setItem('viewed_incidents_ids', JSON.stringify(ids));
}

const ENDPOINT = 'incidents';

const mapStateToProps = (state) => {
  return {
    ...mapEndpointStateToProps(ENDPOINT, state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...mapEndpointDispatchToProps(ENDPOINT, dispatch),
    handleDateRangeChange(date_range) {
      let dr = {
        $gte: date_range.from,
        $lte: date_range.to
      }
      // if current period < 1 day, show all day incidents
      if (date_range.to - date_range.from < 1000*60*60*24) {
        dr = {
          $gte: moment(date_range.to).subtract(1, 'days').toDate(),
          $lte: date_range.to
        }
      }
      return dispatch(filterSet('incidents', 'created', dr))
    },
    handleRowClick(e, data) {
      saveIdToLocalStorage(data._id)
      dispatch(incidentSelect(data._id))
      dispatch(activeTabSet(2)) // go to the incident tab
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);
