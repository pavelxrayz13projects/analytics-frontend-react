// Actions
import filterSet from '../../modules/endpoints/actions/filter_set';
import filterReplace from '../../modules/endpoints/actions/filter_replace';
import filterRemove from '../../modules/endpoints/actions/filter_remove';
import setFetching from '../../modules/endpoints/actions/set_fetching';
import dataFetch from '../../modules/endpoints/actions/data_fetch';
import attributePlaceAfter from '../../modules/attributes/actions/attribute_place_after';
import attributeToggle from '../../modules/attributes/actions/attribute_toggle';
import attributeSetNewSize from '../../modules/attributes/actions/attribute_set_new_size';
import sortRemove from '../../modules/endpoints/actions/sort_remove';
import sortAscending from '../../modules/endpoints/actions/sort_ascending';
import sortDescending from '../../modules/endpoints/actions/sort_descending';
import pageSet from '../../modules/endpoints/actions/page_set';
import maxResultsSet from '../../modules/endpoints/actions/max_results_set';

const paginationParams = (links={}) => {
  const { prev={}, next={}, last={} } = links
  const findPage = (href='') => {
    const [ page ] = /\d+$/.exec(href) || []
    return parseInt(page, 10);
  };
  return {
    prev: findPage(prev.href) || 1,
    next: findPage(next.href),
    last: findPage(last.href),
  }
}

export const mapEndpointStateToProps = (endp, state) => {
  const ENDPOINT = endp
  const { endpoints, attributes } = state
  const endpoint = endpoints[ENDPOINT]
  const attrs = attributes[ENDPOINT]
  return {
    prefix: ENDPOINT,
    date_range: state.date_range,
    attributes: attrs,
    columns: _(attrs).select(attr => attr.visibility === true),
    items: endpoint.data._items,
    params: endpoint.params,
    isFetching: endpoint.isFetching,
    isSortedAsc(column) {
      return endpoint.params.sort[column.id] === 1;
    },
    isSortedDesc(column) {
      return endpoint.params.sort[column.id] === -1;
    },
    isFiltered(column) {
      const { filter } = endpoint.params
      if (column.id === 'group') {
        return filter.group.$in.length > 0;
      }
      return ((typeof filter[column.id] !== 'undefined') && filter[column.id] !== null)
    },
    getFilter(column) {
      return endpoint.params.filter[column.id];
    },
    getChoices(column) {
      if (column.filter_type !== 'subset') return false
      return endpoint.data._meta.choices[column.id]
    },
    pagination: {
      total: endpoint.data._meta.total || 0,
      max_results: endpoint.params.max_results,
      page: endpoint.params.page,
      ...paginationParams(endpoint.data._links)
    }
  }
}

export const mapEndpointDispatchToProps = (endpoint, dispatch) => {
  return {
    handleRowClick(e, data) {

    },
    handleDateRangeChange(date_range) {
      
    },
    dataFetch(params = {}) {
      dispatch(dataFetch(endpoint, params));
    },
    moveColumn(source, target) {
      dispatch(attributePlaceAfter(endpoint, source, target));
    },
    resizeColumn(id, newSize) {
      dispatch(attributeSetNewSize(endpoint, id, newSize))
    },
    sortAscending(column, isSortedAsc=false) {
      if (isSortedAsc) return dispatch(sortRemove(endpoint, column.id))
      return dispatch(sortAscending(endpoint, column.id))
    },
    sortDescending(column, isSortedDesc=false) {
      if (isSortedDesc) return dispatch(sortRemove(endpoint, column.id))
      return dispatch(sortDescending(endpoint, column.id))
    },
    removeSorting(column) {
      return dispatch(sortRemove(endpoint, column.id))
    },
    attrVisibilityToggle(e, id) {
      dispatch(attributeToggle(endpoint, id));
    },
    setPage(page) {
      if(isNaN(page)) return false;
      dispatch(pageSet(endpoint, page));
    },
    setMaxResults(maxResults) {
      dispatch(maxResultsSet(endpoint, maxResults));
    },
    handleFilterForm(column, data) {
      dispatch(pageSet(endpoint, 1));
      dispatch(filterSet(endpoint, column.id, data))
    },
    handleResetFilterForm(column) {
      dispatch(pageSet(endpoint, 1));
      dispatch(filterRemove(endpoint, column.id))
    },
  }
}
