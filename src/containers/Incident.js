import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { _t } from '../constants'

// Widgets
import Collapsible from '../widgets/Collapsible'
import IncidentCard from '../widgets/IncidentCard'

// Actions
import filterReplace from '../modules/endpoints/actions/filter_replace'

const getIncident = (data=[], _id=null) => {
  if (data.length === 0 || _id === null) return false
  return _(data).findWhere({ _id })
}

const mapStateToProps = (state) => {
  return {
    data: getIncident(
      state.endpoints.incidents.data._items,
      state.endpoints.incidents.selectedId
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    selectNext() {

    },
    selectPrev() {

    },
    setEventsFilter(logdate, sig_text) {
      dispatch(filterReplace('events', {
        logdate: { $gte: logdate },
        sig_text
      }))
    }
  }
}

class Incident extends Component {
  render() {
    const { data, setEventsFilter } = this.props
    if (!data) return <div>Empty</div>
    return (
      <Collapsible title={_t("Incident")} collapsed={false}>
        <IncidentCard
          data={data}
          handleEventClick={setEventsFilter}
        />
      </Collapsible>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Incident);
