import "react-day-picker/lib/style.css";
import '../public/assets/css/date-range-picker.css';

import React, { Component, PropTypes } from "react";
import { connect } from 'react-redux';
import moment from "moment";
import DayPicker, { DateUtils } from "react-day-picker";
import LocaleUtils from "react-day-picker/moment";
import classnames from 'classnames';
import "moment/locale/ru";
import { dateTimeFormat } from '../constants';

// Actions
import dateRangeSet from '../modules/date_range/actions/date_range_set'
import dateRangeSetByPeriod from '../modules/date_range/actions/date_range_set_by_period'

const mapStateToProps = ({ date_range }) => {
  return {
    date_range
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setDateRange(date_range) {
      dispatch(dateRangeSet(date_range))
    },
    setDateRangeByPeriod(period, unit='minutes') {
      dispatch(dateRangeSetByPeriod({ period, unit }))
    }
  }
}

class DatePickerContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      period: 15,
      from: null,
      to: null,
    }
  }

  componentDidMount() {
    const fn = () => {
      if (this.state.period !== null) {
        this.props.setDateRangeByPeriod(this.state.period)
      }
    }
    this.timerID = setInterval(fn, 60000)
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  handleDayClick(e, day) {
    const range = DateUtils.addDayToRange(day, this.state)
    if (range.from === null && range.to === null) {
      range.from = new Date(day)
      range.to = new Date(day)
    }
    if (range.from && range.to === null) range.to = new Date(range.from)
    if (range.from) range.from = moment(range.from).startOf('day').toDate()
    if (range.to) range.to = moment(range.to).endOf('day').toDate()
    this.props.setDateRange(range)
    this.setState({
      period: null,
      ...range
    })
  }

  handlePeriodClick(period) {
    this.setState({ period });
    this.props.setDateRangeByPeriod(period)
    this.setState({
      from: null,
      to: null
    });
  }

  render() {
    const modifiers = {
      selected: day => DateUtils.isDayInRange(day, this.state)
    }
    return (
      <div className="DatePickerContainer">
        <DayPicker
          ref="daypicker"
          localeUtils={ LocaleUtils }
          locale="ru"
          numberOfMonths={ 2 }
          modifiers={ modifiers }
          onDayClick={ this.handleDayClick.bind(this) }
        />
        <div className="DatePickerContainer-periods">
          <span className={classnames({'dpc-period--active': this.state.period === 15})} onClick={() => this.handlePeriodClick(15)}>15 минут</span>
          <span className={classnames({'dpc-period--active': this.state.period === 60})} onClick={() => this.handlePeriodClick(60)}>60 минут</span>
          <span className={classnames({'dpc-period--active': this.state.period === 24*60})} onClick={() => this.handlePeriodClick(24*60)}>24 часа</span>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DatePickerContainer);
