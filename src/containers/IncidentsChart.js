import 'chartist/dist/chartist.min.css'

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import Chartist from 'chartist'

import { dateTimeFormat } from '../constants'

// Actions
import incidentsChartFetchData from '../modules/charts/actions/incidents_chart_fetch_data'
import incidentsChartSetData from '../modules/charts/actions/incidents_chart_set_data'

const calcStep = (params) => {
  const i = params.to - params.from
  if (i <= 60*61*1000 ) return 1
  return 30
  // const k = 100
  // return Math.ceil(i / 1000 / 60 / k)
}

const makeTimeLine = (date_from, date_to, step=30) => {
  if (date_to > new Date) date_to = new Date
  const timelineValuesQty = Math.ceil((date_to - date_from) / 1000 / 60 / step)
  const _res = []
  let slider = date_from
  let i = 0
  const r = 15 // Visible values on the timeline
  while(slider <= date_to) {
    if (i % (Math.ceil(timelineValuesQty / r)) === 0) {
       _res.push(moment(slider).format(dateTimeFormat))
    } else {
      _res.push('')
    }
    slider = moment(slider).add(step, 'minutes').toDate()
    i++
  }
  return _res
}

const mapStateToProps = ({ date_range, incidentsChart }) => {
  return {
    data: incidentsChart.data,
    params: date_range
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchIncidentsChartData(params) {
      dispatch(incidentsChartFetchData(params))
    },
    setIncidentsChartData(data) {
      dispatch(incidentsChartSetData(data))
    }
  }
}

class IncidentsChart extends Component {
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.params) !== JSON.stringify(this.props.params)) {
      if (this.canPlot(nextProps.params)) {
        this.setStep(calcStep(nextProps.params))
        this.props.fetchIncidentsChartData({
          ...nextProps.params,
          // to: moment(nextProps.params.to).add(1, 'minutes').toDate(),
          step: this.getStep()
        })
      } else {
        this.props.setIncidentsChartData()
      }
    }
    if (JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data)) {
      const timeLine = makeTimeLine(nextProps.params.from, nextProps.params.to, this.getStep())
      this.chart.update({
        labels: timeLine,
        series: [nextProps.data],
      })
    }
  }
  canPlot(params) {
    return params.to - params.from < 1000*60*60*24*7 // week
  }
  setStep(step) {
    this.step = step
  }
  getStep() {
    return this.step
  }
  componentDidMount() {
    this.chart = new Chartist.Bar('#incidents-chart', {
      labels: [],
      series: [[]],
    }, {
      fullWidth: true,
      chartPadding: {
        right: 90
      }
    });
    this.setStep(calcStep(this.props.params))
    this.props.fetchIncidentsChartData({...this.props.params, step: this.getStep()})
  }
  render() {
    return (
      <div id="incidents-chart"></div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IncidentsChart);
