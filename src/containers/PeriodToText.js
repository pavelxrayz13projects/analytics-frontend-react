import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { dateTimeFormat } from '../constants'
moment.locale('ru')

// const toText = (date_range) => {
//   if (period > 0) return 'за ' + moment().subtract(period, 'milliseconds').fromNow(true);
//   const dateToString = (d) => moment(d).format('YYYY-MM-DD HH:mm:ss');
//   return `с ${dateToString(date_range.from)} по ${dateToString(date_range.to)}`;
// }

const mapStateToProps = ({ date_range }) => {
  return {
    date_range
  }
}

const PeriodToText = ({ date_range }) => (
  <div className="period-to-text">
    с {moment(date_range.from).format(dateTimeFormat)} по {moment(date_range.to).format(dateTimeFormat)}
  </div>
)

export default connect(mapStateToProps)(PeriodToText);
