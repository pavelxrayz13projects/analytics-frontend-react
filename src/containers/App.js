import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

import { _t } from '../constants'

// Widgets
import Collapsible from '../widgets/Collapsible'
import Header from '../widgets/Header/header'
import TopScrollButton from '../widgets/TopScrollButton'

// Containers
import AppStatus from './AppStatus'
import AppFilters from './AppFilters'
import Titles from './Titles'
import PeriodToText from './PeriodToText'
import Incident from './Incident'
import TabsSelector from './TabsSelector'

// Endpoint Table Containers
import FiredSignatures from './Tables/FiredSignatures'
import EventsChart from './EventsChart'
import IncidentsChart from './IncidentsChart'
import Events from './Tables/Events'
import Webproxy from './Tables/Webproxy'
import Firewall from './Tables/Firewall'
import Incidents from './Tables/Incidents'

import reducers from '../modules/reducers'

const store = applyMiddleware(thunk)(createStore)(reducers)

const AppContainer = () => {
  return (
    <Provider store={store}>
      <div id='app'>
        <AppStatus />
        <Header />
        <AppFilters/>
        <div className="container">
          <PeriodToText />
          <Collapsible title={_t("Event classes")} collapsed={false}>
            <Titles />
          </Collapsible>
          <Collapsible title={_t("Events chart")} collapsed={false}>
            <EventsChart />
          </Collapsible>
          <Collapsible title={_t("Incidents chart")} collapsed={false}>
            <IncidentsChart />
          </Collapsible>
          <TabsSelector tabs={[_t('fired_signatures'), _t('incidents')]}>
            <FiredSignatures />
            <Incidents />
            <Incident />
          </TabsSelector>
          <Events />
          <Webproxy />
          <Firewall />
          <TopScrollButton />
        </div>
      </div>
    </Provider>
  )
}

export default AppContainer;
