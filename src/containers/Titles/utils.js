import React from 'react';

export const getTitleIcon = (group, count, coefficient) => {
  // console.log(coefficient);
  const icon_bad = <i className="material-icons rotate-45 error">add_circle</i>;
  const icon_warning = <i className="material-icons warning">warning</i>;
  const icon_warning_gray = <i className="material-icons gray">warning</i>;
  const icon_good = <i className="material-icons good">check_circle</i>;

  if (count < 0) return icon_warning_gray;
  if (count === 0) return icon_good;

  switch (group) {
    case 'auth':
      if (count > 20 * coefficient) return icon_warning;
      if (count > 50 * coefficient) return icon_bad;
      return icon_good;
    case 'info':
      return icon_good;
    case 'malware':
    case 'attacks':
    case 'ddos':
    case 'incidents':
      return icon_bad;
    default:
      return icon_warning;
  }
}
