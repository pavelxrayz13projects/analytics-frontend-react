import './titles.css'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

// Widgets
import Title from './title'

// Actions
import countersFetch from '../../modules/counters/actions/counters_fetch'
import counters1Fetch from '../../modules/counters/actions/counters1_fetch'

const mapStateToProps = ({ date_range }) => {
  return {
    date_range
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCounters(date_range) {
      const i = date_range.to - date_range.from
      const date_range_before = {
        from: moment(date_range.from).subtract(i, 'milliseconds').toDate(),
        to: new Date(date_range.from)
      }
      dispatch(countersFetch(date_range))
      return dispatch(counters1Fetch(date_range_before))
    }
  }
}

class TitlesContainer extends Component {
  componentDidMount() {
    const { fetchCounters, date_range } = this.props
    return fetchCounters(date_range)
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.date_range) !== JSON.stringify(this.props.date_range)) {
      const { fetchCounters, date_range } = nextProps
      return fetchCounters(date_range)
    }
  }
  render() {
    return (
      <div className="titles">
        <div className="titles-row clearfix">
          <Title group="info" />
          <Title group="scan" />
          <Title group="auth" />
          <Title group="policy" />
          <Title group="malware" />
          <Title group="ddos" />
          <Title group="attacks" />
          <Title group="other" />
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TitlesContainer)
