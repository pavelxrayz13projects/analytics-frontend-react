import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import ajaxLoader from '../../public/assets/images/ajax-loader.gif';

import { getTitleIcon } from './utils';
import { _t } from '../../constants';

// Actions
import grpToggle from '../../modules/groups/actions/group_toggle'
import groupToggle from '../../modules/endpoints/actions/group_toggle'
import fsToggleGroup from '../../modules/fired_signatures/actions/fs_toggle_group'

const calcPercent = (current=0, previous) => {
  if (typeof previous === 'undefined') return
  return Math.ceil(current * 100 / previous - 100)
}

const mapStateToProps = ({ counters, counters1, date_range }, { group }) => {
  // if (counters1._items.length > 0) console.log(counters1);
  let { count: item_count }  = _(counters._items).findWhere({ group }) || {};
  let { count: item1_count } = _(counters1._items).findWhere({ group }) || {};

  return {
    group,
    coefficient: Math.floor((date_range.to - date_range.from ) / 1000*60*15),
    qty: typeof item_count === 'undefined' ? 0 : item_count,
    isFetching: counters.isFetching,
    ratio: calcPercent(item_count, item1_count)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleGroupFilter(group) {
      dispatch(grpToggle(group))
      dispatch(groupToggle(group));
      dispatch(fsToggleGroup(group));
    }
  }
}

const IconDecrease = <i className="material-icons good">arrow_drop_down</i>
const IconIncrease = <i className="material-icons error">arrow_drop_up</i>
const Ratio = ({ value }) => (
  <div className="title-ratio">
    { value <= 0 ? IconDecrease : IconIncrease }
    {Math.abs(value)}%
  </div>
)

const NoRatio = () => (
  <div className="title-ratio">
    <i className="material-icons">remove</i>
  </div>
)

class Title extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelected: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  static propTypes = {
    group: PropTypes.string.isRequired,
    qty: PropTypes.number.isRequired,
    isFetching: PropTypes.bool.isRequired,
    toggleGroupFilter: PropTypes.func.isRequired
  }

  handleClick(e) {
    // if (this.props.qty < 0) return false;
    this.setState({ isSelected: !this.state.isSelected });
    this.props.toggleGroupFilter(this.props.group);
  }

  render() {
    const { group, qty, isFetching, ratio, coefficient } = this.props;
    const { isSelected } = this.state;
    const loader = <img src={ajaxLoader} />

    return (
      <div className="titles-item">
        <div
          className={classnames(['titles-title', { 'title--selected': isSelected }])}
          onClick={this.handleClick}
        >
          <div className="title-group">
            {_t(group)}
          </div>
          <div className="title-main">
            { isFetching ? loader : getTitleIcon(group, qty, coefficient) }
            { qty >= 0 ? qty : 'N/A' }
          </div>
          { ratio ? <Ratio value={ratio}/> : <NoRatio /> }
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Title);
