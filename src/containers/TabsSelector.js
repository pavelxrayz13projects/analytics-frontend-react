import React from 'react'
import { connect } from 'react-redux'

// Widgets
import Tabs from '../widgets/Tabs'

// Actions
import activeTabSet from '../modules/active_tab/actions/active_tab_set'

const mapStateToProps = ({ active_tab }, { tabs }) => {
  return {
    active_tab,
    tabs
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setTab(tabIndex){
      dispatch(activeTabSet(tabIndex))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tabs)
