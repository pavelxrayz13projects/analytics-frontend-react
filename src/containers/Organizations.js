import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { getSensors } from '../modules/utils'
import { _t } from '../constants'

// Widgets
import TreeMenu from '../widgets/TreeMenu/tree_menu'
import TreeBranch from '../widgets/TreeMenu/tree_branch'
import TreeLeaf from '../widgets/TreeMenu/tree_leaf'

// Actions
import organizationsFetch from '../modules/organizations/actions/organizations_fetch'
import sensorToggle from '../modules/sensors/actions/sensor_toggle'
import sensorsToggle from '../modules/sensors/actions/sensors_toggle'
import filterSet from '../modules/endpoints/actions/filter_set'

const mapStateToProps = ({ organizations, sensors }) => {
  return {
    sensors,
    organizations: organizations._items,
    relatedSensors(obj) {
      return getSensors(obj)
    },
    isChecked(address) {
      if (Array.isArray(address)) {
        const address_string = address.sort().join(',')
        const sensors_string = _(sensors).intersection(address).sort().join(',')
        return sensors_string === address_string
      }
      return sensors.indexOf(address) !== -1
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOrganizations() {
      dispatch(organizationsFetch())
    },
    toggleSensor(sensor) {
      if (Array.isArray(sensor)) {
        return dispatch(sensorsToggle(sensor))
      }
      dispatch(sensorToggle(sensor))
    },
    setFilter(sensors) {
      return dispatch(filterSet('events', 'sensor_ip', {
        $in: sensors
      }))
    }
  }
}

class Organizations extends Component {
  componentDidMount() {
    this.props.fetchOrganizations()
  }
  render() {
    const { sensors, organizations, toggleSensor, relatedSensors, isChecked, setFilter } = this.props
    return (
      <TreeMenu>
        {organizations.map((org, orgIndex) =>
          <TreeBranch
            key={orgIndex}
            id={`${orgIndex}-tree-branch`}
            title={`${org.name} (${org.branches.length})`}
            handleChange={(e) => toggleSensor(relatedSensors(org))}
            isChecked={isChecked(relatedSensors(org))}
          >
            {org.branches.map((branch, branchIndex) =>
              <TreeBranch
                key={branchIndex}
                id={`${orgIndex}-${branchIndex}-tree-branch`}
                title={`${branch.name} (${branch.segments.length})`}
                handleChange={(e) => toggleSensor(relatedSensors(branch))}
                isChecked={isChecked(relatedSensors(branch))}
              >
                {branch.segments.map((segment, segmentIndex) =>
                  <TreeBranch
                    key={segmentIndex}
                    id={`${orgIndex}-${branchIndex}-${segmentIndex}-tree-branch`}
                    title={`${segment.name} (${segment.sensors.length})`}
                    handleChange={(e) => toggleSensor(relatedSensors(segment))}
                    isChecked={isChecked(relatedSensors(segment))}
                  >
                    {segment.sensors.map((sensor, sensorIndex) =>
                      <TreeLeaf
                        key={sensorIndex}
                        id={`${orgIndex}-${branchIndex}-${segmentIndex}-${sensorIndex}-tree-leaf`}
                        title={`${sensor.type} - ${sensor.address} / ${sensor.address}`}
                        handleChange={(e) => toggleSensor(sensor.address)}
                        isChecked={isChecked(sensor.address)}
                      />
                    )}
                  </TreeBranch>
                )}
              </TreeBranch>
            )}
          </TreeBranch>
        )}
        <li style={{textAlign: 'center', paddingTop: '16px'}}>
          <a href="javascript:void(0)" onClick={() => setFilter(sensors)}>
            {_t('Apply filter')}
          </a>
        </li>
      </TreeMenu>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Organizations)
