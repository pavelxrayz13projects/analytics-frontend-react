import React, { Component, PropTypes } from "react"
import { connect } from 'react-redux'
import { getSensors } from '../modules/utils'

// widgets
import AppFilters from '../widgets/AppFilters/app_filters'

// Actions
import dateRangeSet from '../modules/date_range/actions/date_range_set'
import dateRangeSetByPeriod from '../modules/date_range/actions/date_range_set_by_period'
import organizationsFetch from '../modules/organizations/actions/organizations_fetch'
import sensorToggle from '../modules/sensors/actions/sensor_toggle'
import sensorsToggle from '../modules/sensors/actions/sensors_toggle'
import filterSet from '../modules/endpoints/actions/filter_set'
import fsSetAggregate from '../modules/fired_signatures/actions/fs_set_aggregate'

const mapStateToProps = ({ organizations, sensors, date_range }) => {
  return {
    sensors,
    date_range,
    organizations: organizations._items,
    relatedSensors(obj) {
      return getSensors(obj)
    },
    isChecked(address) {
      if (Array.isArray(address)) {
        const address_string = address.sort().join(',')
        const sensors_string = _(sensors).intersection(address).sort().join(',')
        return sensors_string === address_string
      }
      return sensors.indexOf(address) !== -1
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOrganizations() {
      dispatch(organizationsFetch())
    },
    toggleSensor(sensor) {
      if (Array.isArray(sensor)) {
        return dispatch(sensorsToggle(sensor))
      }
      dispatch(sensorToggle(sensor))
    },
    setSensors(sensors) {
      dispatch(fsSetAggregate({ 'sensor_ip': {
        $in: sensors
      }}))
      return dispatch(filterSet('events', 'sensor_ip', {
        $in: sensors
      }))
    },
    setDateRange(date_range) {
      dispatch(dateRangeSet(date_range))
    },
    setDateRangeByPeriod(period, unit='minutes') {
      dispatch(dateRangeSetByPeriod({ period, unit }))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppFilters)
