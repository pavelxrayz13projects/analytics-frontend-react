import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {

  }
};

const mapDispatchToProps = (dispatch) => {
  return {

  }
};

class Dummy extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  static propTypes = {
    p: PropTypes.func.isRequired,
  };
  render() {
    return (

    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dummy);
