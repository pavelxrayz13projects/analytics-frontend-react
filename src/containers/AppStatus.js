import style from '../public/assets/css/app-state-alert.css'

import React from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'

import appStateSetOk from '../modules/app_state/actions/app_state_set_ok'

const mapStateToProps = ({ app_state }) => {
  return {
    app_state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearError: () => dispatch(appStateSetOk())
  }
}

const AppStatusContainer = ({ app_state, clearError }) => {
  const { error = {} } = app_state
  const { data = {} } = error
  return (
    <div className={classnames(['app-state-alert', 'fade', { in: app_state.status > 200 }])}>
      <a href="javascript:void(0)" onClick={clearError}><i className="material-icons">close</i></a>
      {' '}
      <span>[{ error.status }]</span>
      {' '}
      <span>{ data.message || error.statusText }</span>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(AppStatusContainer);
