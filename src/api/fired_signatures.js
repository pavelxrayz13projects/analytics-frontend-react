var Router = require('express/lib/router');
var bodyParser = require('body-parser');
var url = require('url');
var _ = require('underscore');
var axios = require('axios');
var moment = require('moment');

var router = new Router();

var EVE_BACKEND_URL = `http://${process.env.EVE_BACKEND_HOST}:${process.env.EVE_BACKEND_PORT}`;

router.post('/', bodyParser.json(), (req, res) => {

  var page = req.body.page || 1;
  var max_results = req.body.max_results || 25;
  var aggregate = req.body.aggregate;

  var params = {
    page: parseInt(page, 10),
    max_results: parseInt(max_results, 10),
  }

  if (undefined !== aggregate) {
    if (aggregate.group && aggregate.group.$in && aggregate.group.$in.length === 0) {
      delete aggregate.group;
    }
    params.aggregate = JSON.stringify(aggregate);
  }

  var paramsString = _.pairs(params).map(p => `${p[0]}=${p[1]}`).join('&');

  var url = `${EVE_BACKEND_URL}/fired_signatures?${paramsString}`;

  if (process.env.NODE_ENV === 'development') {
    console.log(url);
  }

  axios.get(url)
    .then(result => res.status(200).send(result.data))
    .catch(err => {
      return res.status(err.status).send({
        status: err.status,
        statusText: err.statusText,
        message: err.data._error.message
      });
    });
});

module.exports = router;
