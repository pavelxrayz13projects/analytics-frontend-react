var Router = require('express/lib/router');
var bodyParser = require('body-parser');
var url = require('url');
var _ = require('underscore');
var axios = require('axios');
var moment = require('moment');

var router = new Router();

var EVE_BACKEND_URL = `http://${process.env.EVE_BACKEND_HOST}:${process.env.EVE_BACKEND_PORT}`;

router.post('/', bodyParser.json(), (req, res) => {
  var message = '';

  var params = {
    $from: req.body.date_from || req.body.from || req.body.$from,
    $to: req.body.date_to || req.body.to || req.body.$to,
    $step: req.body.step
  }

  if (typeof params.$step === 'undefined' || params.$step < 1) message = 'Wrong step value, ' + params.$step;
  if (typeof params.$from === 'undefined' || typeof params.$to === 'undefined') message = 'Wrong date range';
  if (message.length > 0) return res.status(500).send({ message: message });

  var url = `${EVE_BACKEND_URL}/incidents_histogram?aggregate=${JSON.stringify(params)}`;

  if (process.env.NODE_ENV === 'development') {
    console.log(url);
  }

  axios.get(url)
    .then(result => res.status(200).send(result.data._items))
    .catch(err => {
      return res.status(err.status).send({
        status: err.status,
        statusText: err.statusText,
        message: err.data._error.message
      });
    });
});

module.exports = router;
