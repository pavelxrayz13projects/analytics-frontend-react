var Router = require('express/lib/router');
var bodyParser = require('body-parser');
var axios = require('axios');

var router = new Router();

var EVE_BACKEND_URL = `http://${process.env.EVE_BACKEND_HOST}:${process.env.EVE_BACKEND_PORT}`;

router.post('/', bodyParser.json(), (req, res) => {

  var url = `${EVE_BACKEND_URL}/groups/group-list`;

  if (process.env.NODE_ENV === 'development') {
    console.log(url);
  }

  axios.get(url)
    .then(result => res.status(200).send(result.data.groups))
    .catch(err => {
      return res.status(err.status).send({
        status: err.status,
        statusText: err.statusText,
        message: err.data._error.message
      });
    });
});

module.exports = router;
