var Router = require('express/lib/router');
var bodyParser = require('body-parser');
var url = require('url');
var _ = require('underscore');
var axios = require('axios');

var router = new Router();

var EVE_BACKEND_URL = `http://${process.env.EVE_BACKEND_HOST}:${process.env.EVE_BACKEND_PORT}`;

router.param('endpoint', (req, res, next, endpoint) => {
  req.endpoint_url = `${EVE_BACKEND_URL}/${endpoint}`;
  next();
});

router.post('/:endpoint', bodyParser.json(), (req, res) => {

  var page = req.body.page;
  var max_results = req.body.max_results;
  var sort = req.body.sort;
  var filter = req.body.filter;

  var params = {
    page: parseInt(page, 10),
    max_results: parseInt(max_results, 10)
  }

  if (undefined !== sort) {
    params.sort = '[' + _.pairs(sort).map(chunk => `("${chunk[0]}", ${chunk[1]})`).join(',') + ']';
  }

  if (undefined !== filter) {
    if (filter.group && filter.group.$in && filter.group.$in.length === 0) {
      delete filter.group;
    }
    if (filter.sensor_ip && filter.sensor_ip.$in && filter.sensor_ip.$in.length === 0) {
      delete filter.sensor_ip;
    }
    params.where = JSON.stringify(filter);
  }

  var paramsString = _.pairs(params).map(p => `${p[0]}=${p[1]}`).join('&');

  var url = `${req.endpoint_url}?${paramsString}`;

  if (process.env.NODE_ENV === 'development') {
    console.log(url);
  }

  axios.get(url)
    .then(result => {
      return res.status(200).send(result.data)
    })
    .catch(err => {
      return res.status(err.status).send({
        status: err.status,
        statusText: err.statusText,
        message: err.data._error.message
      });
    });
});

module.exports = router;
