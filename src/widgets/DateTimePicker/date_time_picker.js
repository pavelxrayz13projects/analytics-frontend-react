import './style.css'
import React, { Component, PropTypes } from "react"
import { connect } from 'react-redux'
import moment from "moment"
import "moment/locale/ru"
import { dateTimeFormat } from '../../constants'
import classnames from 'classnames'
import DayPicker, { DateUtils } from "react-day-picker"
import LocaleUtils from "react-day-picker/moment"

const TimePicker = ({ handleClick }) => {
  const timeArray = []
  for (let i = 0; i < 24; i++) {
    timeArray[i] = i
  }
  return(
    <div className="time-picker">
      {timeArray.map(i =>
        <div key={i}>
          <div className="time-picker-item" onClick={e => handleClick(i, 0)}>{i}:00</div>
          <div className="time-picker-item" onClick={e => handleClick(i, 30)}>{i}:30</div>
        </div>
      )}
      <div className="time-picker-item" onClick={e => handleClick(23, 59)}>23:59</div>
    </div>
  )
}

export default class DateTimePicker extends Component {
  static propTypes = {
    handleChange: PropTypes.func.isRequired
  }
  constructor(props) {
    super(props)
    this.state = {
      selectedDay: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.date === null) {
      this.timeInp.value = ''
      return
    }
    if (this.props.date === null || nextProps.date.toString() !== this.props.date.toString()) {
      this.timeInp.value = moment(nextProps.date).format(dateTimeFormat)
    }
  }
  render() {
    let pickerDate, pickerTime

    const setTime = (h, m) => {
      let prevTime = new Date(this.timeInp.value)
      if (prevTime.toString() === 'Invalid Date') {
        prevTime = new Date
      }
      const currentTime = moment(prevTime)
      currentTime.hour(h)
      currentTime.minute(m)
      this.timeInp.value = currentTime.format(dateTimeFormat)
      this.props.handleChange(currentTime.toDate())
    }
    const setDate = (e, day, { selected }) => {
      this.setState({ selectedDay: selected ? null : day })
      let prevTime = new Date(this.timeInp.value)
      const currentTime = moment(new Date(day))
      if (prevTime.toString() !== 'Invalid Date') {
        prevTime = moment(prevTime)
        currentTime.hour(prevTime.hour())
        currentTime.minute(prevTime.minute())
      }
      this.timeInp.value = currentTime.startOf('day').format(dateTimeFormat)
      this.props.handleChange(currentTime.toDate())
    }
    const getDefaultValue = (date) => {
      if (date === null) return ''
      let d = new Date(date)
      if (d.toString() === 'Invalid Date') return ''
      return moment(date).format(dateTimeFormat)
    }
    return (
      <div className="date-time-picker">
        <input
          type="text"
          ref={node => this.timeInp = node}
          defaultValue={getDefaultValue(this.props.date)}
          onBlur={e => this.props.handleChange(new Date(e.currentTarget.value))}
        />
        <div className="picker-wrapper">
          <div className="picker-tabs">
            <span
              className="picker-tab-date"
              onClick={e => {
                pickerDate.style.display = 'block'
                pickerTime.style.display = 'none'
              }}
            >
              <i className="material-icons">date_range</i>
            </span>
            <span
              className="picker-tab-time"
              onClick={e => {
                pickerDate.style.display = 'none'
                pickerTime.style.display = 'block'
              }}
            >
              <i className="material-icons">av_timer</i>
            </span>
          </div>
          <div className="picker-date" ref={node => pickerDate = node}>
            <DayPicker
              ref="daypicker"
              localeUtils={ LocaleUtils }
              locale="ru"
              numberOfMonths={ 1 }
              onDayClick={ setDate }
              selectedDays={day => DateUtils.isSameDay(this.state.selectedDay, day)}
            />
          </div>
          <div className="picker-time" ref={node => pickerTime = node}>
            <TimePicker handleClick={ setTime }/>
          </div>
        </div>
      </div>
    )
  }
}
