import './filters.css';

import React, { Component } from 'react';
import classnames from 'classnames';
import { _t } from '../../constants';
import moment from 'moment'

// widgets
import DateTimePeriodFilter from './date_time_period_filter'
import OrganizationsFilter from './organizations_filter'

class AppFilters extends Component {
  constructor(props) {
    super(props)
    this.state = {
      period: 15,
      from: null,
      to: null,
      scrolled: false,
      collapsed: true
    }
    this.applyFilters = this.applyFilters.bind(this)
    this.toggleCollapse = this.toggleCollapse.bind(this)
    this.handleScroll = this.handleScroll.bind(this)
  }
  componentDidMount() {
    this.props.fetchOrganizations()
    window.addEventListener('scroll', this.handleScroll)
    this.startLiveUpdate(this.state.period)
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
    clearInterval(this.timerID)
  }
  startLiveUpdate(period) {
    clearInterval(this.timerID)
    if (period === null) return false
    const fn = () => {
      this.props.setDateRangeByPeriod(period)
    }
    this.timerID = setInterval(fn, 60000)
    this.props.setDateRangeByPeriod(period)
  }
  stopLiveUpdate() {
    clearInterval(this.timerID)
  }
  toggleCollapse() {
    this.setState({ collapsed: !this.state.collapsed });
  }
  handleScroll(e) {
    // this.setState({ scrolled: (window.pageYOffset >= 88) })
  }
  applyFilters() {
    this.props.setSensors(this.props.sensors)
    this.startLiveUpdate(this.state.period)
    if (this.state.period === null) {
      this.props.setDateRange({
        from: this.state.from,
        to: this.state.to
      })
    }
    this.toggleCollapse()
  }
  render() {
    return (
      <div className={classnames([
          "filters",
          { "filters--fixed": this.state.scrolled },
          { 'filters--collapsed': this.state.collapsed },
        ])}>
        <div className='filters-body clearfix'>
          <div className="filter-items-row">
            <div className="filter-item">
              <OrganizationsFilter
                sensors={this.props.sensors}
                organizations={this.props.organizations}
                toggleSensor={this.props.toggleSensor}
                relatedSensors={this.props.relatedSensors}
                isChecked={this.props.isChecked}
              />
            </div>
            <div className="filter-item">
              <DateTimePeriodFilter
                period={this.state.period}
                from={this.state.from}
                to={this.state.to}
                setPeriod={p => this.setState({ from: null, to: null, period: p })}
                setFrom={f => this.setState({ from: f, period: null })}
                setTo={t => this.setState({ to: t, period: null })}
              />
            </div>
          </div>
        </div>
        <div className="filters-control">
          <a
            href="javascript:void(0)"
            className={classnames({ hidden: this.state.collapsed })}
            onClick={this.applyFilters}
          >
            {_t('Apply filters')}{' '}
            <i className="material-icons rotate-90">chevron_left</i>
          </a>
          <a
            href="javascript:void(0)"
            className={classnames({ hidden: !this.state.collapsed })}
            onClick={this.toggleCollapse}
          >
            {_t('Show filters')}{' '}
            <i className="material-icons rotate-90">chevron_right</i>
          </a>
        </div>
      </div>
    )
  }
}

export default AppFilters
