import React from "react"
import moment from 'moment'

// Widgets
import DateTimePicker from '../DateTimePicker'
import Button from '../Button'

const DateTimePeriodFilter = ({
  period,
  from,
  to,
  setPeriod,
  setFrom,
  setTo
}) => (
  <div className="date-time-period-filter">
    <div>Выберите интервал</div>
    <div className="sep">с</div>
    <DateTimePicker date={from} handleChange={setFrom} />
    <div className="sep">по</div>
    <DateTimePicker date={to} handleChange={setTo}/>
    <div className="sep">или за период</div>
    <div>
      <Button onClick={(e) => {
          setTo(new Date)
          setFrom(moment().subtract(15, 'minutes').toDate())
        }}>
        15 м
      </Button>{' '}
      <Button onClick={(e) => {
          setTo(new Date)
          setFrom(moment().subtract(60, 'minutes').toDate())
        }}>
        60 м
      </Button>{' '}
      <Button onClick={(e) => {
          setTo(new Date)
          setFrom(moment().subtract(24, 'hours').toDate())
        }}>
        24 ч
      </Button>
    </div>
    <div className="sep">или за период (автообновление)</div>
    <div>
      <Button isActive={period === 15} onClick={(e) => setPeriod(15)}>15 м <i className="material-icons">update</i></Button>{' '}
      <Button isActive={period === 60} onClick={(e) => setPeriod(60)}>60 м <i className="material-icons">update</i></Button>{' '}
      <Button isActive={period === 60*24} onClick={(e) => setPeriod(60*24)}>24 ч <i className="material-icons">update</i></Button>
    </div>
  </div>
)

export default DateTimePeriodFilter
