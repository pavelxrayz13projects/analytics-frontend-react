import React from 'react'

// Widgets
import TreeMenu from '../TreeMenu/tree_menu'
import TreeBranch from '../TreeMenu/tree_branch'
import TreeLeaf from '../TreeMenu/tree_leaf'

const OrganizationsFilter = ({
  sensors,
  organizations,
  toggleSensor,
  relatedSensors,
  isChecked
}) => (
  <TreeMenu>
    {organizations.map((org, orgIndex) =>
      <TreeBranch
        key={orgIndex}
        id={`${orgIndex}-tree-branch`}
        title={`${org.name} (${org.branches.length})`}
        handleChange={(e) => toggleSensor(relatedSensors(org))}
        isChecked={isChecked(relatedSensors(org))}
      >
        {org.branches.map((branch, branchIndex) =>
          <TreeBranch
            key={branchIndex}
            id={`${orgIndex}-${branchIndex}-tree-branch`}
            title={`${branch.name} (${branch.segments.length})`}
            handleChange={(e) => toggleSensor(relatedSensors(branch))}
            isChecked={isChecked(relatedSensors(branch))}
          >
            {branch.segments.map((segment, segmentIndex) =>
              <TreeBranch
                key={segmentIndex}
                id={`${orgIndex}-${branchIndex}-${segmentIndex}-tree-branch`}
                title={`${segment.name} (${segment.sensors.length})`}
                handleChange={(e) => toggleSensor(relatedSensors(segment))}
                isChecked={isChecked(relatedSensors(segment))}
              >
                {segment.sensors.map((sensor, sensorIndex) =>
                  <TreeLeaf
                    key={sensorIndex}
                    id={`${orgIndex}-${branchIndex}-${segmentIndex}-${sensorIndex}-tree-leaf`}
                    title={`${sensor.type} - ${sensor.address} / ${sensor.address}`}
                    handleChange={(e) => toggleSensor(sensor.address)}
                    isChecked={isChecked(sensor.address)}
                  />
                )}
              </TreeBranch>
            )}
          </TreeBranch>
        )}
      </TreeBranch>
    )}
  </TreeMenu>
)

export default OrganizationsFilter
