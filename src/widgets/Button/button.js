import './style.css'
import React, { Component, PropTypes } from "react"
import classnames from 'classnames'

const Button = ({ isActive=false, children, onClick }) => (
  <span className={classnames([
    'button',
    { active: isActive }
  ])} onClick={onClick}>
  {children}
  </span>
)

export default Button
