import React, { Component } from 'react'

import { _t } from '../../constants'
import ActionButtons from './action_buttons'
import { parseFormValues, clearForm, getRangeFilter } from './utils'

class SubsetFilterForm extends Component {
  constructor(props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
    this.onReset = this.onReset.bind(this)
  }
  onSubmit() {
    const $in = [...this.form.elements]
      .filter((el) => el.nodeName === 'INPUT' && el.checked)
      .map((el) => isNaN(parseInt(el.value, 0)) ? el.value : parseInt(el.value, 0));

    if ($in.length > 0) {
      return this.props.onSubmit({ $in });
    } else {
      this.onReset()
    }
    return false;
  }
  onReset() {
    const formElements = [...this.form.elements];
    for (let elemIndex in formElements) {
      formElements[elemIndex].checked = true
    }
    this.props.onReset()
  }
  render() {
    // console.log(this.props.filter.$in)
    // checked={this.props.filter.indexOf(item) !== -1}
    const hasFilters = this.props.filter
      && Array.isArray(this.props.filter.$in)
      && this.props.filter.$in.length > 0
      
    return (
      <form
        ref={node => this.form = node}
        onSubmit={this.onSubmit}
      >
        {this.props.choices.map((item, index) =>
          <div key={index} className="dd-menu-item">
            <input
              type="checkbox"
              defaultChecked={!hasFilters || this.props.filter.$in.indexOf(item) !== -1}
              name='subset[]'
              value={item}
              id={`subset_${this.props.prefix}_${index}`}
            />
            <label htmlFor={`subset_${this.props.prefix}_${index}`}>{_t(item)}</label>
          </div>
        )}
        <ActionButtons onSubmit={this.onSubmit} onReset={this.onReset} />
      </form>
    )
  }
}

export default SubsetFilterForm;
