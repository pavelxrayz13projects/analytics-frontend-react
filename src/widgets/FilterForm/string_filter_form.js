import React, { Component, PropTypes } from 'react'
import ActionButtons from './action_buttons'
import { parseFormValues, clearForm } from './utils'
import { _t } from '../../constants'

class StringFilterForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      lastModified: 'regex',
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onReset = this.onReset.bind(this)
  }
  fillForm(filter) {
    if (typeof filter !== 'undefined') {
      if (typeof filter['$regex'] !== 'undefined') {
        this.form.regex.value = filter['$regex']
        this.setState({ lastModified: 'regex' })
      } else {
        this.form.substring.value = filter
        this.setState({ lastModified: 'substring'})
      }
    }
  }
  componentDidMount() {
    this.fillForm(this.props.filter)
  }
  componentWillReceiveProps(nextProps) {
    if(JSON.stringify(nextProps.filter) !== JSON.stringify(this.props.filter)) {
      this.fillForm(nextProps.filter)
    }
  }
  onSubmit(e) {
    e.preventDefault();
    const values = parseFormValues(this.form, val => val.trim())
    const { lastModified } = this.state;
    if ('regex' === lastModified
        && 'undefined' !== typeof values.regex
        && '' !== values.regex)
    {
      this.form.substring.value = ''
      return this.props.onSubmit({
        '$regex': values.regex,
        '$options': 'i'
      })
    }
    if ('substring' === lastModified
        && 'undefined' !== typeof values.substring
        && '' !== values.substring)
    {
      this.form.regex.value = ''
      return this.props.onSubmit(values.substring)
    }
    return false;
  }
  onReset() {
    clearForm(this.form)
    this.props.onReset()
  }
  render() {
    return (
      <form
        ref={node => this.form = node}
        onSubmit={this.onSubmit}
      >
        <div className="dd-menu-item--filter">
          <input type="text" name="regex" placeholder={_t("Regex")} onChange={() => this.setState({lastModified: 'regex'})} />
        </div>
        <div className="separator"/>
        <div className="dd-menu-item--filter">
          <input type="text" name="substring" placeholder={_t("Substring")} onChange={() => this.setState({lastModified: 'substring'})} />
        </div>
        <ActionButtons onSubmit={this.onSubmit} onReset={this.onReset} />
      </form>
    )
  }
}

export default StringFilterForm;
