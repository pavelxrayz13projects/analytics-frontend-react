import './header.css'

import React from 'react'
import logo from './logo.svg'

const Header = () => (
  <div className="header">
    <div className="header-brand clearfix">
      <img src={logo} alt="logo" />
      <span>Threat Intelligence</span><br/>
      <span className="header-brand--small">analytics system</span>
    </div>
    <a
      style={{
        position: 'absolute',
        top: 0,
        right: 0,
        color: '#0288d1',
        cursor: 'default',
      }}
      href="javascript:localStorage.clear();window.location.href='/'"
      title="clear localStorage"
    >
      clear localStorage
    </a>
  </div>
)

export default Header
