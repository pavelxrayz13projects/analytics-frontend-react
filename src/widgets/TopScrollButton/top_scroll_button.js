import './style.css'

import React, { Component } from 'react'
import classnames from 'classnames'
import { _t } from '../../constants'

export default class TopScrollButton extends Component {
  constructor(props) {
    super(props)
    this.state = {
      scrolled: false,
    }
    this.handleScroll = this.handleScroll.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }
  handleScroll(e) {
    this.setState({ scrolled: (window.pageYOffset >= 88) })
  }
  handleClick(e) {
    window.scrollTo(0, 0);
  }
  render() {
    const { scrolled } = this.state
    return (
      <div
        className={classnames(["top-scroll-button", { scrolled }])}
        onClick={this.handleClick}
        title={_t('Scroll to top')}
      >
        <i className="material-icons">arrow_upward</i>
      </div>
    )
  }
}
