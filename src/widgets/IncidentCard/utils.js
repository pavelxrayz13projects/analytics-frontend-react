export const RenderList = (item, key) => {
  if (typeof item === 'string') return <li key={key}>{item}</li>;
  if (Array.isArray(item)) return item.map((i, j) => RenderList(i, j));
  if (typeof item === 'object') {
    return Object.keys(item).map(subitem => {
      return (
        <li key={subitem}>{_t(subitem)}
          <ul>{RenderList(item[subitem])}</ul>
        </li>
      )
    });
  }
}
