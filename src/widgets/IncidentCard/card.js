import './style.css';

import React from 'react';
import moment from 'moment';
import { _t, dateTimeFormat } from '../../constants';

const formatDate = (str) => moment(new Date(str)).format(dateTimeFormat)

const IDSEventsTable = ({ caption, data, handleEventClick }) => {
  const rows = []
  data.map(d => {
    d.events.map((e, ei) => {
      rows.push(
        <tr key={ei} onClick={() => handleEventClick(e.date_fact, e.message)}>
          <td>{formatDate(e.date_fact)}</td>
          <td style={{maxWidth: '200px'}} title={e.message}>{e.message}</td>
          <td>{d.ip}</td>
          <td>{d.sensor}</td>
          <td>{e.src_mac}</td>
          <td>{e.src_address}</td>
          <td>{e.dst_mac}</td>
          <td>{e.dst_address}</td>
          <td>
            <a target="_blank" href={e.pcap_link}>
              <i className="material-icons">file_download</i>
            </a>
          </td>
          <td>{e.event_id}</td>
        </tr>
      )
    })
  })
  return (
    <table>
      <caption>{caption}</caption>
      <thead>
        <tr>
          <th>{_t('date_fact')}</th>
          <th>{_t('message')}</th>
          <th>{_t('sensor_ip')}</th>
          <th>{_t('sensor_name')}</th>
          <th>{_t('src_mac')}</th>
          <th>{_t('src_address')}</th>
          <th>{_t('dst_mac')}</th>
          <th>{_t('dst_address')}</th>
          <th>{_t('pcap_link')}</th>
          <th>{_t('event_id')}</th>
        </tr>
      </thead>
      <tbody>
        {rows}
      </tbody>
    </table>
  )
}

const pluralazeAffectedActives = (qty) => {
  if (qty > 1 && qty < 5) return `${qty} пораженных узла`
  if (qty === 1 || (qty > 11 && (qty - 1) % 10 === 0)) return `${qty} пораженный узел`
  return `${qty} пораженных узлов`
}

const Card = ({ data, handleEventClick }) => {
  return (
    <div className="icard">

      <div className="icard-row">
        <div className="icard-item">
          <h3><span className="icard-id">{data.level}</span>#{data._id}</h3>
        </div>
        <div className="icard-item">
          <h3><i className="material-icons">person</i> {_t('impact')}</h3>
          <ul>
            <li>Внутренний сегмент</li>
            <li>{pluralazeAffectedActives(data.affected_actives.length)}</li>
          </ul>
        </div>
        <div className="icard-item">
          <h3><i className="material-icons">date_range</i> {_t('detecting date')}</h3>
          <ul>
            <li>{formatDate(data.created)}</li>
          </ul>
        </div>
      </div>

      <div className="icard-row">
        <h4>{data.title}</h4>
        <p>
          {data.summary}
        </p>
      </div>

      <div className="icard-row">
        <div className="icard-item">
          <h3>{_t('affected_actives')}</h3>
          <ul>
            <li>Выявлено {pluralazeAffectedActives(data.affected_actives.length)}:</li>
          </ul>
        </div>
        <div className="icard-item">
          <h3>{_t('impact methods')}</h3>
          <ul>
            {data.threats.map((t, ti) => <li key={ti}>{t}</li>)}
          </ul>
        </div>
        <div className="icard-item">
          <h3>{_t('symptoms')}</h3>
          <ul>
            {data.symptoms.map((s, si) => <li key={si}>{s}</li>)}
          </ul>
        </div>
      </div>

      <div className="icard-row">
        {data.affected_actives.map((a, ai) =>
          <div key={ai} className="icard-item">
            <h5><i className="material-icons">desktop_windows</i> msk-03-dd</h5>
            <ul>
              <li>User: User</li>
              <li>ip: {a.ip}</li>
              <li>mac: {a.mac}</li>
            </ul>
          </div>
        )}
      </div>

      <div className="icard-row">
        <div className="icard-item recommendations">
          <h3><i className="material-icons error">add_to_queue</i> {_t('recommendations')}</h3>
          <ul>
            {data.recommendations.map((r, ri) => <li key={ri}>{r}</li>)}
          </ul>
        </div>
      </div>

      <div className="icard-tables">
        <h2>{_t('additional information')}</h2>
        <IDSEventsTable caption={_t('main_events')} data={data.main_events} handleEventClick={handleEventClick}/>
        <IDSEventsTable caption={_t('correlated_events')} data={data.correlated_events} handleEventClick={handleEventClick}/>
      </div>
    </div>
  )
}

export default Card;
