import React, { Component }  from 'react';
import style from './style.css';
import classnames from 'classnames';

class Collapsible extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: true
    };
    this.collapse = this.collapse.bind(this)
  }
  componentDidMount() {
    const { collapsed=true } = this.props
    this.setState({ collapsed })
  }
  collapse() {
    this.setState({ collapsed: !this.state.collapsed })
  }
  render() {
    const classNames = classnames([
      "collapsible",
      "card",
      { "collapsible--collapsed": this.state.collapsed }
    ])
    return (
      <div className={classNames}>
        <div onClick={this.collapse} className="collapsible-bar clearfix">
          <div className="collapsible-title">
            {this.props.title}
          </div>
          <i className="collapsible-action-icon material-icons">chevron_left</i>
        </div>
        <div className="collapsible-body">
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default Collapsible;
