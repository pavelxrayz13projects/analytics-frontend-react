import React from 'react'

const TreeLeaf = ({
  id,
  title,
  handleChange,
  isChecked
}) => (
  <li className="tree-leaf">
    <input
      id={id}
      type="checkbox"
      onChange={handleChange}
      checked={isChecked}
    />
    <label htmlFor={id}>{title}</label>
  </li>
)

export default TreeLeaf
