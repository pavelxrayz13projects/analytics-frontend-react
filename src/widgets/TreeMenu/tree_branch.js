import React, { Component, PropTypes } from 'react'
import classnames from 'classnames'

export default class TreeBranch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      collapsed: true
    }
    this.collapse = this.collapse.bind(this)
  }
  collapse(e) {
    this.setState({ collapsed: !this.state.collapsed })
  }
  render(){
    const { collapsed } = this.state
    const { id, title, handleChange, isChecked } = this.props
    return (
      <li className={classnames(["tree-branch", { collapsed }])}>
        <i
          className="material-icons collapse-branch"
          onClick={this.collapse}
        >
          { this.state.collapsed ? 'add' : 'remove' }
        </i>
        <input
          type="checkbox"
          id={id}
          onChange={handleChange}
          checked={isChecked}
        />
        <label htmlFor={id}>
          {title}
        </label>
        <ul>
          {this.props.children}
        </ul>
      </li>
    )
  }
}
