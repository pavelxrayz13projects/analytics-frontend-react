import './tree.css'
import React from 'react'

const TreeMenu = ({ children }) => (
  <ul className="tree-menu">{children}</ul>
)

export default TreeMenu
