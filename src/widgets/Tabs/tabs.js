import './styles.css'

import React from 'react'
import classnames from 'classnames'

const Tabs = ({ tabs, active_tab, setTab, children }) => (
  <div className='tabs'>
    <div className='tabs-buttons'>
      {tabs.map((tab, tabIndex) =>
        <div
          key={tabIndex}
          className={classnames(['tabs-button', {active: active_tab === tabIndex}])}
          onClick={() => setTab(tabIndex)}
        >
          <span>{tab}</span>
        </div>
      )}
    </div>
    <div className='tabs-body'>
    {children.map((child, childIndex) =>
      <div
        className={classnames(['tab', {hidden: active_tab !== childIndex}])}
        key={childIndex}
      >
        {child}
      </div>
    )}
    </div>
  </div>
)

export default Tabs
