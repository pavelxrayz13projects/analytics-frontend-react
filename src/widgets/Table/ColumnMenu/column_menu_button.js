import React from 'react'
import classnames from 'classnames'

const ColumnMenuButton = ({
  isMenuButtonVisible,
  onMenuButtonClick
}) => (
  <a
    href="javascript:void(0)"
    className={classnames(["filter-menu-link", {"hidden": !isMenuButtonVisible}])}
    onClick={(e) => {
      onMenuButtonClick();
      e.stopPropagation(); // prevent closing filter widget
    }}
  >
    <i className="material-icons">menu</i>
  </a>
)

export default ColumnMenuButton
