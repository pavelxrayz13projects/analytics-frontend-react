import './dd-menu.css'

import React from 'react'
import classnames from 'classnames'

const ColumnMenu = ({
  children,
  isVisible
}) => (
  <div
    className={classnames(["dd-menu", {"hidden": !isVisible}])}
  >
    {children}
  </div>
)

export default ColumnMenu
