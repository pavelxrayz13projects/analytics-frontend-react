import React from 'react'
import classnames from 'classnames'

const MenuItem = ({ title, icon, onClick, selected, children }) => {
  // let subMenu;
  // if (typeof children !== 'undefined') subMenu = <div className="dd-menu">{children}</div>
  return (
    <div
      onClick={onClick}
      className={classnames([
        "dd-menu-item",
        "dd-menu-item--highlight",
        {"dd-menu-item--has-submenu": (typeof children !== 'undefined')},
        {"dd-menu-item--selected": selected}
      ])}
    >
      <span className="pull-left">{title}</span>
      <span className="pull-right">{icon}</span>
      {children}
    </div>
  )
}

export default MenuItem
