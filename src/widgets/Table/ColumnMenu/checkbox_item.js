import React from 'react'

const CheckboxItem = ({ prefix, checked, label, id, onChange }) => (
  <div
    className="dd-menu-item dd-menu-item--highlight"
  >
    <input
      checked={checked}
      type="checkbox"
      id={`dd-${prefix}-item-${id}`}
      onChange={(e) => onChange(e, id)}
    />
    <label htmlFor={`dd-${prefix}-item-${id}`}>{label}</label>
  </div>
)

export default CheckboxItem
