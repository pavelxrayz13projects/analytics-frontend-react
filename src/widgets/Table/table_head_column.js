import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'

// Widgets
import TableHeadColumnResizable from './table_head_column_resizable'

class TableHeadColumn extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
  }
  render() {
    let style = {}
    if (this.props.width) style = { ...style, width: this.props.width }
    return (
      <th ref="head_col" style={style}>
        {this.props.children}
      </th>
    )
  }
}

export default TableHeadColumn;
