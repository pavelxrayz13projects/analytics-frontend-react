import React from 'react';
import classnames from 'classnames';

const TableBodyRow = ({ columns, row, onRowClick }) => {
  return (
    <tr
      onClick={() => onRowClick(row)}
      className={classnames({ active: rowIndex === this.state.activeRowIndex })}
    >
      {columns.map((column, colIndex) => {
        if (row[column.id] === undefined || row[column.id] === null) return <td key={colIndex}></td>;
        return (
          <Cell
            key={colIndex}
            type={column.type}
            width={column.width}
          >
            {row[column.id]}
          </Cell>
        )
      })}
    </tr>
  )
}
