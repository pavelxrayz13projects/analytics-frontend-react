import React, { Component, PropTypes } from 'react'
import { findDOMNode } from 'react-dom';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from '../../constants';
import classnames from 'classnames'

const columnResizerTarget = {
  hover(props, monitor, component) {
    const resizer = monitor.getItem()
    const positionX = monitor.getClientOffset().x //; console.log(positionX); // current x position
    const minPositionX = resizer.headColumnRect.x + 10
    const newSize = (positionX > minPositionX ? positionX : minPositionX) - resizer.headColumnRect.x
    console.log(newSize);
    // console.log(component);
    // console.log('getBoundingClientRect', findDOMNode(component).getBoundingClientRect())
    props.resizeColumn(resizer.id, newSize);
  },
  drop(props, monitor, component) {
    const newSize = monitor.getItem().newSize;
    const id = props.id;
    // console.log(props);
    // console.log(monitor.getItem());
    // console.log(monitor.didDrop());
    // console.log(monitor.getDropResult());
    //props.resizeColumn(id, newSize);
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class TableHeadRowDropTarget extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
  }
  render() {
    const { isOver } = this.props
    return this.props.connectDropTarget(
      <tr className={classnames(["column-row-drop-target", { isOver }])}>
        {this.props.children}
      </tr>
    )
  }
}

export default DropTarget(ItemTypes.COLUMN_RESIZER, columnResizerTarget, collect)(TableHeadRowDropTarget)
