import './css/table.css';
import './css/table-collapsible.css';
import './css/column-title-dnd.css';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { findDOMNode } from 'react-dom';
import classnames from 'classnames';

import { _t } from '../../constants';

// Widgets
import TableHead from './table_head';
import TableHeadRowDropTarget from './table_head_row_drop_target'
import TableHeadColumn from './table_head_column';
import TableHeadColumnTitleDnD from './table_head_column_title_dnd'
import TableBody from './table_body';
import Pagination from './Pagination';
import ColumnMenu from './ColumnMenu/column_menu';
import MenuItem from './ColumnMenu/menu_item';
import CheckboxItem from './ColumnMenu/checkbox_item';
import FilterForm from '../FilterForm';

class Table extends Component {
  constructor(props) {
    super(props)
    this.state = {
      collapsed: true,
      columnsMenuVisibility: false,
      openedfilterMenuId: null,
      column: {}
    }
    this.closeAllMenus = this.closeAllMenus.bind(this)
    this.toggleColumnsMenu = this.toggleColumnsMenu.bind(this)
    this.toggleFilterMenu = this.toggleFilterMenu.bind(this)
    this.toggleTable = this.toggleTable.bind(this)
    this.moveFilterMenuContainer = this.moveFilterMenuContainer.bind(this)
  }
  componentDidMount() {
    if (['webproxy', 'firewall'].indexOf(this.props.prefix) === -1) {
      this.props.dataFetch(this.props.params)
    }
    if (['incidents', 'fired_signatures'].indexOf(this.props.prefix) !== -1) {
      this.setState({ collapsed: false })
    }
    //window.addEventListener('click', this.closeAllMenus);
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.params) !== JSON.stringify(this.props.params)) {
      return this.props.dataFetch(nextProps.params);
    }
    if (JSON.stringify(nextProps.date_range) !== JSON.stringify(this.props.date_range)) {
      return this.props.handleDateRangeChange(nextProps.date_range)
    }
  }
  componentWillUnmount() {
    //window.removeEventListener('click', this.closeAllMenus);
  }
  toggleColumnsMenu() {
    this.setState({ columnsMenuVisibility: !this.state.columnsMenuVisibility })
  }
  toggleFilterMenu(id) {
    if (id === this.state.openedfilterMenuId) {
      return this.setState({ openedfilterMenuId: null })
    }
    this.setState({ openedfilterMenuId: id })
  }
  toggleTable() {
    this.setState({ collapsed: !this.state.collapsed })
    if (!this.state.collapsed) {
      this.setState({
        columnsMenuVisibility: false,
        openedfilterMenuId: null
      })
    }
  }
  closeAllMenus() {
    this.setState({
      columnsMenuVisibility: false,
      openedfilterMenuId: null
    })
  }
  moveFilterMenuContainer(e) {
    const w = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;
    const mostLeftX = w - 356;
    const positionX = e.currentTarget.getBoundingClientRect().left - 50; //console.log(positionX);
    // console.log(e.currentTarget.getBoundingClientRect());
    const resPositionX = positionX < mostLeftX ? positionX : mostLeftX;
    this.filterMenuContainer.style.left = `${resPositionX}px`;
  }
  render() {
    const {
      sortAscending, isSortedAsc,
      sortDescending, isSortedDesc,
      removeSorting
    } = this.props;
    return (
      <div className={classnames(["table-collapsible card", `${this.props.prefix}-table-collapsible`,{collapsed: this.state.collapsed}])}>
        <div className="table-bar clearfix" onClick={this.toggleTable}>
          <div className="table-title">
            { _t(this.props.prefix) }
          </div>
          <div
            className={classnames([
              "table-culumn-filter menu",
              { hidden: this.state.openedfilterMenuId === null }
            ])}
            onClick={e => e.stopPropagation()}
            ref={node => this.filterMenuContainer = node}
          >
            <FilterForm
              prefix={`${this.props.prefix}_${this.state.column.filter_type}`}
              filterType={this.state.column.filter_type}
              filter={this.props.getFilter(this.state.column)}
              choices={this.props.getChoices(this.state.column)}
              onSubmit={(data) => {
                this.props.handleFilterForm(this.state.column, data)
                this.setState({ openedfilterMenuId: null })
              }}
              onReset={() => {
                this.props.handleResetFilterForm(this.state.column)
                this.setState({ openedfilterMenuId: null })
              }}
            />
          </div>
          <div className="table-menu">
            <div className="table-menu-columns">
              <i
                className={classnames(["material-icons", {hidden: this.state.collapsed}])}
                title={_t("Select columns")}
                onClick={e => {
                  e.stopPropagation()
                  this.toggleColumnsMenu()
                }}
              >more_vert</i>
              <div
                className={classnames(["columns-menu menu", {hidden: !this.state.columnsMenuVisibility}])}
                onClick={e => e.stopPropagation()}
              >
                {this.props.attributes.map((attr, attrIndex) =>
                  <CheckboxItem
                    key={attrIndex}
                    prefix={this.props.prefix}
                    checked={attr.visibility}
                    label={_t(attr.id)}
                    id={attr.id}
                    onChange={this.props.attrVisibilityToggle}
                  />
                )}
              </div>
            </div>
            <i
              className="table-menu-collapse material-icons"
              title={_t("Collapse/Expand")}
              >chevron_left</i>
          </div>
        </div>
        <div
          className="table-main"
          onScroll={() => this.setState({ openedfilterMenuId: null })}
        >
          <table className={`${this.props.prefix}-table`}>
            <TableHead>
              <TableHeadRowDropTarget resizeColumn={this.props.resizeColumn}>
                {this.props.columns.map((column, colIndex) =>
                  <TableHeadColumn
                    key={colIndex}
                    id={column.id}
                    width={column.width || 0}
                  >
                    <TableHeadColumnTitleDnD
                      id={column.id}
                      filtered={this.props.isFiltered(column)}
                      menuOpened={this.state.openedfilterMenuId === column.id}
                      sortedAsc={isSortedAsc(column)}
                      sortedDesc={isSortedDesc(column)}
                      moveColumn={this.props.moveColumn}
                    >
                      <i
                        className="material-icons filter-icon"
                        title={_t("Apply filter")}
                        onClick={(e) => {
                          this.setState({ column })
                          this.moveFilterMenuContainer(e)
                          this.toggleFilterMenu(column.id)
                        }}
                      >filter_list</i>
                      <span
                        className="column-title"
                        title={_t("Sort")}
                        onClick={() => {
                          if (isSortedAsc(column) + isSortedDesc(column) === 0) {
                            return sortAscending(column)
                          }
                          if (isSortedAsc(column)) {
                            return sortDescending(column)
                          }
                          if (isSortedDesc(column)) {
                            return removeSorting(column)
                          }
                        }}
                      >{_t(column.id)}</span>
                      <span className="html-icon sort-asc-icon">&uarr;</span>
                      <span className="html-icon sort-desc-icon">&darr;</span>
                    </TableHeadColumnTitleDnD>
                  </TableHeadColumn>
                )}
              </TableHeadRowDropTarget>
            </TableHead>
            <TableBody
              prefix={this.props.prefix}
              isFetching={this.props.isFetching}
              columns={this.props.columns}
              data={this.props.items}
              onRowClick={this.props.handleRowClick}
            />
          </table>
        </div>
        <div className="table-pagination">
          <Pagination
            {...this.props.pagination}
            setMaxResults={this.props.setMaxResults}
            setPage={this.props.setPage} />
        </div>
        <div className="incident-wrapper"></div>
      </div>
    )
  }
}

export default Table
