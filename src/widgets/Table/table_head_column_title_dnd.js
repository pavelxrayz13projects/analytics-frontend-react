import React, { Component, PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import { DragSource, DropTarget } from 'react-dnd'
import classnames from 'classnames'
import { _t, ItemTypes } from '../../constants'

import TableHeadColumnTitleDnDSource from './table_head_column_title_dnd_source'

const columnTitleTarget = {
  drop(props, monitor, component) {
    const source = monitor.getItem().id;
    const target = props.id;
    props.moveColumn(source, target);
  }
};

const collectDrop = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  }
}

class TableHeadColumnTitleDnD extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    filtered: PropTypes.bool.isRequired,
    sortedAsc: PropTypes.bool.isRequired,
    sortedDesc: PropTypes.bool.isRequired,
    menuOpened: PropTypes.bool.isRequired,
    moveColumn: PropTypes.func.isRequired,
    // added by decorator
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
  }
  render() {
    const {
      id,
      filtered,
      sortedAsc,
      sortedDesc,
      menuOpened,
      moveColumn,
      connectDropTarget,
      isOver,
    } = this.props
    return connectDropTarget(
      <div className={classnames([
        "column-title-dnd",
        { filtered,
          sortedAsc,
          sortedDesc,
          menuOpened,
          isOver }
      ])}>
        <TableHeadColumnTitleDnDSource id={id}>
          {this.props.children}
        </TableHeadColumnTitleDnDSource>
      </div>
    )
  }
}

export default DropTarget(ItemTypes.COLUMN_TITLE, columnTitleTarget, collectDrop)(TableHeadColumnTitleDnD);
