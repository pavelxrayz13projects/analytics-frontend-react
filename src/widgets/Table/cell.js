import React from 'react'
import moment from 'moment'
import { _t, _t_for_tables, dateTimeFormat } from '../../constants'

const style = {}

const Cell = ({ id, type, width, children }) => {
  if ('priority,direction,group'.split(',').indexOf(id) !== -1) children = _t_for_tables(children)
  if (type === 'datetime') children = moment(new Date(children)).format(dateTimeFormat)
  if (id === 'affected_actives' && Array.isArray(children)) children = children.map(a => a.ip)
  if (Array.isArray(children)) children = children.join(', ')
  return (
    <td className={type} style={{ ...style, maxWidth: width }}>
      {(() => {
        switch (type) {
          case 'datetime': return <span title={children}>{children}</span>
          case 'link':     return <a href={children} title={children} target="_blank"><i className="material-icons">file_download</i></a>
          default:         return <span title={children}>{children}</span>
        }
      })()}
      <i
        className="material-icons copy-to-clipboard"
        title={_t("Copy")}
        onClick={(e) =>{
          e.stopPropagation()
          window.prompt(_t("Ctrl+C, Enter"), children)
        }}
      >content_copy</i>
    </td>
  )
}

export default Cell
