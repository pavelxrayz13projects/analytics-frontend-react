import React, { Component } from 'react';
import Cell from './cell';
import classnames from 'classnames';

import { _t } from '../../constants';

class TableBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rowIndex: null
    }
  }
  componentWillReceiveProps(nextProps) {
    if (JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data)) {
      this.state.rowIndex = null;
    }
  }
  render() {
    const { isFetching, columns, data, onRowClick, prefix } = this.props;
    let ids = []
    if (prefix === 'incidents' && localStorage.hasOwnProperty('viewed_incidents_ids')) {
      ids = JSON.parse(localStorage.getItem('viewed_incidents_ids'))
    }
    if (isFetching) return <tbody><tr><td colSpan={columns.length}>{_t('Fetching')}...</td></tr></tbody>;
    if (data.length === 0) return <tbody><tr><td colSpan={columns.length}>{_t('No results found')}.</td></tr></tbody>;
    const rows = data.map((row, rowIndex) => {
      const colElems = columns.map((column, colIndex) => {
        if (row[column.id] === undefined || row[column.id] === null) return <td key={colIndex}></td>;
        return (
          <Cell
            key={colIndex}
            id={column.id}
            type={column.type}
            width={column.width}
          >
            {row[column.id]}
          </Cell>
        )
      })
      return (
        <tr
          key={rowIndex}
          onClick={(e) => {
            const isSameRow = this.state.rowIndex === rowIndex;
            onRowClick(e, row, isSameRow)
            if (isSameRow) {
              this.setState({ rowIndex: null })
            } else {
              this.setState({ rowIndex })
            }
          }}
          className={classnames({
            active: rowIndex === this.state.rowIndex,
            viewed: prefix === 'incidents' && ids.indexOf(row._id) !== -1
          })}
        >
          {colElems}
        </tr>
    )});
    return <tbody className={`tbody-endpoint-${prefix}`}>{rows}</tbody>;
  }
}

export default TableBody;
