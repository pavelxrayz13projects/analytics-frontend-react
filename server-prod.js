var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(require('morgan')('short'));

// Serve static files
app.use(express.static(__dirname + '/dist/public/'));

// Register API middleware
app.use('/api/endpoints', require(__dirname + '/src/api/endpoints'));
app.use('/api/counters', require(__dirname + '/src/api/counters'));
app.use('/api/groups', require(__dirname + '/src/api/groups'));
app.use('/api/organizations', require(__dirname + '/src/api/organizations'));
app.use('/api/fired_signatures', require(__dirname + '/src/api/fired_signatures'));
app.use('/api/events_histogram', require(__dirname + '/src/api/events_histogram'));
app.use('/api/incidents_histogram', require(__dirname + '/src/api/incidents_histogram'));

// Root route
app.get("/", function(req, res) {
  res.sendFile(__dirname + '/dist/public/index.html');
});

if (require.main === module) {
  var server = http.createServer(app);
  server.listen(process.env.PORT || 9000, function() {
    console.log("Listening on %j", server.address());
  });
}
